$(function(){
	$("#menu-links li a").click(function(event){
		event.preventDefault();
		href = $(this).attr('href').replace('#', '');

		$('.menu-tab').addClass('hide');
		$('#menu-links li').removeClass('active');
		$(this).parent().addClass('active');
		$('#'+href).removeClass('hide');
	});
});

function loadPromotionalPrices() {
	$('#table-promotion tbody').loading();
	var url = wwwroot + '/products_async/loadPromotionalPrices/'+$('#product_id').val();

	$.ajax({
		url: url,
		success: function(data) {
			$('#table-promotion tbody').loading('stop');
			$('#table-promotion tbody').html(data);
		},
		error: function(a, b, c) {
			console.log(a, b, c);
		}
	});
	
}

// promotional prices
function addPromotionalPrice() {
	var data = {
		product_id: $('#product_id').val()
	};
	openGenericModal('promotional_price', data);
}

function registerPromotionalPrice(e) {
	e.preventDefault();

    var form_data = $('#form-add-promotional-price').serializeArray();

    var url = wwwroot+'/products_async/addPromotionalPrice/'+$('#product_id').val();
    console.log(form_data);

    var posting = $.post(url, form_data);

    posting.done(function(data) {
    	console.log(data);
        if (data.status.code == 200) {
            loadPromotionalPrices();
            $('#generic-modal').modal('hide');
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

// product groups
function loadProductGroups() {
	$('#table-product-groups tbody').loading();
	var url = wwwroot + '/products_async/loadProductGroups/'+$('#product_id').val();

	$.ajax({
		url: url,
		success: function(data) {
			$('#table-product-groups tbody').loading('stop');
			$('#table-product-groups tbody').html(data);
		},
		error: function(a, b, c) {
			console.log(a, b, c);
		}
	});
	
}

function addProductGroup() {
	var data = {
		product_id: $('#product_id').val()
	};
	openGenericModal('product_group', data);
}

function registerProductGroup(e) {
	e.preventDefault();

    var form_data = $('#form-add-product-group').serializeArray();

    var url = wwwroot+'/products_async/addProductGroup/'+$('#product_id').val();

    var posting = $.post(url, form_data);

    posting.done(function(data) {
        if (data.status.code == 200) {
            loadProductGroups();
            $('#generic-modal').modal('hide');
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

// images
function loadImages() {
	$('#table-images tbody').loading();
	var url = wwwroot + '/products_async/loadImages/'+$('#product_id').val();

	$.ajax({
		url: url,
		success: function(data) {
			$('#table-images tbody').loading('stop');
			$('#table-images tbody').html(data);
		},
		error: function(a, b, c) {
			$('#table-images tbody').loading('stop');
			console.log(a, b, c);
		}
	});
	
}

function addImage() {
	var data = {
		product_id: $('#product_id').val()
	};
	openGenericModal('image', data);
}

function registerImage(e) {
	e.preventDefault();

	var url = wwwroot+'/products_async/addImage/'+$('#product_id').val();

	var form_data = new FormData($('#form-add-image')[0]);

    $.ajax({
        url : url, 
        type : 'POST',
        data : form_data,
        processData: false,  // tell jQuery not to process the data
        contentType: false,
        async: false,
        success : function(data){
        	$('#generic-modal').modal('hide');
        	loadImages();
        },
        error: function(a, b, c){
        	console.log(a, b, c);
        }
    });
}

// wholesale price
function loadWholesalePrices() {
    $('#table-wholesale-price tbody').loading();
    var url = wwwroot + '/products_async/loadWholesalePrices/'+$('#product_id').val();

    $.ajax({
        url: url,
        success: function(data) {
            $('#table-wholesale-price tbody').loading('stop');
            $('#table-wholesale-price tbody').html(data);
        },
        error: function(a, b, c) {
            console.log(a, b, c);
        }
    });
    
}

function addWholesalePrice() {
    var data = {
        product_id: $('#product_id').val()
    };
    openGenericModal('wholesale_price', data);
}

function registerWholesalePrice(e) {
    e.preventDefault();

    var form_data = $('#form-add-wholesale-price').serializeArray();

    var url = wwwroot+'/products_async/addWholesalePrice/'+$('#product_id').val();

    var posting = $.post(url, form_data);

    posting.done(function(data) {
        if (data.status.code == 200) {
            loadWholesalePrices();
            $('#generic-modal').modal('hide');
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

// components
function loadComponents() {
    $('#table-components tbody').loading();
    var url = wwwroot + '/products_async/loadComponents/'+$('#product_id').val();

    $.ajax({
        url: url,
        success: function(data) {
            $('#table-components tbody').loading('stop');
            $('#table-components tbody').html(data);
        },
        error: function(a, b, c) {
            console.log(a, b, c);
        }
    });
    
}

function addComponent() {
    var data = {
        product_id: $('#product_id').val()
    };
    openGenericModal('component', data);
}

function registerComponent(e) {
    e.preventDefault();

    var form_data = $('#form-add-component').serializeArray();

    var url = wwwroot+'/products_async/addComponent/'+$('#product_id').val();

    var posting = $.post(url, form_data);

    posting.done(function(data) {
        if (data.status.code == 200) {
            loadComponents();
            $('#generic-modal').modal('hide');
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

// size and color
function loadSizeColors() {
    $('#table-size-color tbody').loading();
    var url = wwwroot + '/products_async/loadSizeColors/'+$('#product_id').val();

    $.ajax({
        url: url,
        success: function(data) {
            $('#table-size-color tbody').loading('stop');
            $('#table-size-color tbody').html(data);
        },
        error: function(a, b, c) {
            console.log(a, b, c);
        }
    });
    
}

function addSizeColor() {
    var data = {
        product_id: $('#product_id').val()
    };
    openGenericModal('size_color', data);
}

function registerSizeColor(e) {
    e.preventDefault();

    var form_data = $('#form-add-size-color').serializeArray();

    var url = wwwroot+'/products_async/addSizeColor/'+$('#product_id').val();

    var posting = $.post(url, form_data);

    posting.done(function(data) {
        if (data.status.code == 200) {
            loadSizeColors();
            $('#generic-modal').modal('hide');
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

/**
 * Utiliza janela modal genérica para vários propósitos na tela do terminal
 */
function openGenericModal(op, data) {
    var modal = $('#generic-modal');
    modal.find('.modal-dialog').removeClass('modal-lg');
    modal.modal('show');
    modal.find(".modal-content").loading();
    switch (op) {
        case 'promotional_price':
            modal.find('.modal-title').html('Novo preço promocional');
            modal.find('.modal-body').load(wwwroot+'/products_async/addPromotionalPrice/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerPromotionalPrice(event);
            });
            break;
        case 'product_group':
            modal.find('.modal-title').html(_lang['new_product_group']);
            modal.find('.modal-body').load(wwwroot+'/products_async/addProductGroup/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerProductGroup(event);
            });
            break;
        case 'image':
            modal.find('.modal-title').html(_lang['new_image']);
            modal.find('.modal-body').load(wwwroot+'/products_async/addImage/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerImage(event);
            });
            break;
        case 'wholesale_price':
            modal.find('.modal-title').html(_lang['new_wholesale_price']);
            modal.find('.modal-body').load(wwwroot+'/products_async/addWholesalePrice/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerWholesalePrice(event);
            });
            break;
        case 'component':
            modal.find('.modal-title').html(_lang['new_component']);
            modal.find('.modal-body').load(wwwroot+'/products_async/addComponent/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerComponent(event);
            });
            break;
        case 'size_color':
            modal.find('.modal-title').html(_lang['new_size_color']);
            modal.find('.modal-body').load(wwwroot+'/products_async/addSizeColor/'+data['product_id'], function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').one('click', function(event){
                $(this).off('click');
                registerSizeColor(event);
            });
            break;
        default:
            break;
    }
}

$(function(){
	loadPromotionalPrices();
	loadProductGroups();
	loadImages();
    loadWholesalePrices();
    loadComponents();
    loadSizeColors();
});