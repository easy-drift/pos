// jQuery class: define campo a ser utilizado pelo teclado virtual
var current_input = $('#product_input');
// string: define a linha da tabela produtos selecionada no momento
var current_product_row = null;
// array: guarda uma lista com os ids das linhas de produtos para navegação com botões
var product_row_list = [];
var currency = $('#currency').val();
/**
 * Recebe e processa valores do teclado virtual
 */
function keypadType(char) {
    /**
     * @param {string} char - caractere escolhido no teclado virtual.
     */
    var regex_int = new RegExp('[0-9]|[\.\,]');
    if (regex_int.test(char)) {
        current_input.val(function() {
            return this.value + char;
        });
    }
    else if (char == '<') {
        // if (current_product_row !== null) {
        //     keypadFunction('delete');
        //     return;
        // }
        current_input.val(function() {
            return this.value.slice(0, -1);
        });
    }
    else if (char == '=') {
        var e = $.Event( "keydown", { keyCode: 13 } );
        current_input.trigger(e);
    }
    else if (char == 'cancel') {
        var e = $.Event( "keydown", { keyCode: 27 } );
        current_input.trigger(e);
    }
}

/*
 * Recebe e processa funções do teclado virtual
 */
var count = 0;
var enter_pressed = false;
function keypadFunction(op) {
    /**
     * @param {string} op - caractere escolhido no teclado virtual.
     */
    var _row = $('#'+current_product_row);
    switch (op) {
        case 'qty':
            if (_row.length <= 0) {
                return;
            }
            _qty_input = _row.find('.row-input.qty');
            _qty_span = _row.find('.row-span.qty');

            current_input = _qty_input;
            // toggle span and input display
            _qty_input.show().val('').focus();
            _qty_span.hide();
            _qty_input.on('keydown', function(event) {
                if (event.keyCode == 13){// && !enter_pressed) {
                    enter_pressed = true;
                    updateProductLine('qty', _row, _qty_input.val());
                }
            });
            break;
        case 'unit_price':
            if (_row.length <= 0) {
                return;
            }
            _price_input = _row.find('.row-input.unit_price');
            _price_span = _row.find('.row-span.unit_price');

            _price_input.val('');

            current_input = _price_input;
            // toggle span and input display
            _price_input.show().focus();
            _price_span.hide();
            _price_input.on('keydown', function(event) {
                if (event.keyCode == 13 && !enter_pressed) {
                    enter_pressed = true;
                    updateProductLine('unit_price', _row, _price_input.val());
                }
            });
            break;
        case 'discount':
            if (_row.length <= 0) {
                return;
            }
            _discount_input = _row.find('.row-input.discount');
            _discount_span = _row.find('.row-span.discount');

            _discount_input.val('');

            current_input = _discount_input;
            // toggle span and input display
            _discount_input.show().focus();
            _discount_span.hide();
            _discount_input.on('keydown', function(event) {
                if (event.keyCode == 13 && !enter_pressed) {
                    enter_pressed = true;
                    updateProductLine('discount', _row, _discount_input.val());
                }
            });
            break;
        case 'delete':
            if (_row.length <= 0) {
                return;
            }
            updateProductLine('delete', _row, false);
            break;
        case 'details':
            if (_row.length <= 0) {
                return;
            }
            count++;
            var params = {
                id: _row.data('orderlineid')
            };
            openGenericModal('details', params);

            break;
        case 'order_details':
            openGenericModal('order_details');
            break;
        case 'park_order':
            // check if order has products
            if ($('#product_table tbody').find('tr').length == 0) {
                openGenericModal('search_parked_order');
            }
            else {
                openGenericModal('park_order');
            }
            break;
        case 'gift_card':
            searchProducts(3);
            break;
        case 'print':
            printOrder();
            break;
        case 'cashout':
            openGenericModal('cashout');
            break;
        case 'deposit':
            openGenericModal('deposit');
            break;
        case 'order_discount':
            openGenericModal('order_discount');
            break;
        default:
            break;
    }
}

/*
 * Atualiza uma linha de produto específico na tabela de produtos
 */
function updateProductLine(op, row, value) {
    /**
     * @param {string} op - operação a ser executada
     * @param {jQuery object} row - linha a ser afetada
     * @param {string} value - valor a ser operado
     */
    _qty_input = row.find('.row-input.qty');
    _unit_price_input = row.find('.row-input.unit_price');
    _discount_input = row.find('.row-input.discount');
    _total_price_input = row.find('.row-input.total_price');
    _total_price_span = row.find('.row-span.total_price');
    switch (op) {
        case 'qty':

            total_price = (parse(_unit_price_input.val(), 'float') * value) - parse(_discount_input.val(), 'float');

            _total_price_input.val(total_price);
            _total_price_span.text(currency+total_price.formatMoney(2, ',', '.'));

            // atualiza o produto em order_lines
            var url = wwwroot+'/terminal_async/updateProductLine';
            var params = {
                order_line_id: row.data('orderlineid'),
                op: 'qty',
                qty: value
            };
            var posting = $.post(url, params);

            posting.done(function(data){
                updateTotal();
            })
            .fail(function(a, b, c){
                console.log(a, b, c);
            });

            current_input = $('#product_input');
            break;
        case 'unit_price':
            value = parse(value, 'float');

            total_price = (parse(_qty_input.val(), 'float') * value) - parse(_discount_input.val(), 'float');

            _total_price_input.val(total_price);
            _total_price_span.text(currency+total_price.formatMoney(2, ',', '.'));

            // atualiza o produto em order_lines
            var url = wwwroot+'/terminal_async/updateProductLine';
            var params = {
                order_line_id: row.data('orderlineid'),
                op: 'unit_price',
                unit_price: value
            };
            var posting = $.post(url, params);

            posting.done(function(data){
                updateTotal();
                _unit_price_input.val(value.formatMoney(2));
            })
            .fail(function(a, b, c){
                console.log(a, b, c);
            });

            current_input = $('#product_input');
            break;
        case 'discount':
            // verify if it's a percent discount
            if (value.indexOf('%') >= 0) {
                value_temp = value.substring(0, value.indexOf('%'));

                total_temp = parse(_qty_input.val(), 'float') * parse(_unit_price_input.val(), 'float');

                value = total_temp * (parse(value_temp, 'float') / 100);
                _discount_input.val(value);
            }
            else {
                value = parse(value, 'float');
            }

            // _discount_input.val(value).hide();
            // _discount_span.text(currency+value.formatMoney(2, ',', '.')).show();

            total_price = parse(_qty_input.val(), 'float') * parse(_unit_price_input.val(), 'float');
            total_price -= parse(_discount_input.val(), 'float');

            _total_price_input.val(total_price);
            _total_price_span.text(currency+total_price.formatMoney(2, ',', '.'));

            // atualiza o produto em order_lines
            var url = wwwroot+'/terminal_async/updateProductLine';
            var params = {
                order_line_id: row.data('orderlineid'),
                op: 'discount',
                discount: parse(_discount_input.val(), 'float')
            };
            var posting = $.post(url, params);

            posting.done(function(data){
                updateTotal();
                _discount_input.val(value.formatMoney(2));
            })
            .fail(function(a, b, c){
                console.log(a, b, c);
            });

            current_input = $('#product_input');
            break;
        case 'delete':
            // atualiza o produto em order_lines
            var url = wwwroot+'/terminal_async/updateProductLine';
            var params = {
                order_line_id: row.data('orderlineid'),
                op: 'delete',
                qty: false
            };
            var posting = $.post(url, params);

            posting.done(function(data){
                row.remove();
                updateTotal();
            })
            .fail(function(a, b, c){
                console.log(a, b, c);
            });
            break;
        default:
            break;
    }

    // update vat
    updateProductLineVat(row);
}

function updateProductLineVat(row) {
    // get total price
    _total_price = row.find('.row-input.total_price').val();
    // get mva prcent
    _tax = row.find('.row-input.tax').val();
    //console.log(_total_price, _tax);

    // get mva value
    tax_total = _total_price * (_tax/100);
    row.find('.row-input.tax_total').val(tax_total);
    // atualiza o produto em order_lines
    var url = wwwroot+'/terminal_async/updateProductLine';
    var params = {
        order_line_id: row.data('orderlineid'),
        op: 'vat',
        vat: tax_total
    };
    var posting = $.post(url, params);

    posting.done(function(data){
        updateTotal();
    })
    .fail(function(a, b, c){
        console.log(a, b, c);
    });
}

/**
 * Procura produtos no banco de dados e os escreve na tabela principal
 * baseado em vários parâmetros como nome, código de barras, 
 * código do produto, etc.
 */
function searchProducts(type_product) {
    if (typeof type_product == 'undefined') {
        type_product = 1;
    }
    var val = $('#product_input').val();

    $("#product-modal").modal('show');
    $("#product-modal .modal-content").loading();

    var url = wwwroot+'/terminal_async/searchProducts';
    var params = {
        term: val,
        type_product: type_product
    };

    var posting = $.post(url, params);

    posting.done(function(data){
        $("#product-modal .modal-content").loading('stop');
        $('#product-table-output tbody').html(data);
        
    }).fail(function(a, b, c){
        $("#product-modal .modal-content").loading('stop');
        console.log(a, b, c);
    });
}

/**
 * Seleciona o produto escolhido na busca, coloca na tabela de produtos principal
 * e atualiza order no banco
 */
function selectProduct(product_id, size_color_id, qty, order_line_id, type_product) {
    if (typeof qty == 'undefined') {
        qty = 1;
    }
    if (typeof type_product == 'undefined') {
        type_product = 1;
    }
    if (typeof order_line_id == 'undefined') {
        order_line_id = false;
    }
    $("#product-modal .modal-content").loading();
    if (typeof size_color_id == 'undefined' || size_color_id == null) {
        size_color_id = '';
    }
    var output = $('#product_table > tbody');
    //console.log(wwwroot+'/terminal_async/selectProduct/'+product_id+'/'+size_color_id+'/'+qty+'/'+order_line_id);

    $.ajax({
        // a função assíncrona terminal_async/selectProduct atualiza order/orderlines no banco
        url: wwwroot+'/terminal_async/selectProduct/'+product_id+'/'+size_color_id+'/'+qty+'/'+order_line_id,
        success: function (data) {
            $("#product-modal .modal-content").loading('stop');
            $('#product-modal').modal('hide');
            if (data == 'product_not_found') {
                $.alert({
                    type: 'red',
                    title: 'Erro',
                    content: 'Product não encontrado',
                    theme: 'light'
                });
            }
            else {
                output.append(data);
                $('#product_input').val('');
                updateTotal();

                // asks for product price
                if (order_line_id == false && type_product == 3) {
                    tr = $(data);
                    current_product_row = tr.attr('id');
                    keypadFunction('details');
                }
            }
            
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

/**
 * Utiliza janela modal genérica para vários propósitos na tela do terminal
 */
function openGenericModal(op, data) {
    var modal = $('#generic-modal');
    modal.find('.modal-dialog').removeClass('modal-lg');
    modal.modal('show');
    modal.find(".modal-content").loading();
    switch (op) {
        case 'employee':
            modal.find('.modal-title').html('Escolher Funcionário');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/selectEmployee', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').hide();
            break;
        case 'client':
            modal.find('.modal-dialog').addClass('modal-lg');
            modal.find('.modal-title').html('Procurar Cliente');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/selectClient', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').hide();
            break;
        case 'client-add':
            modal.find('.modal-dialog').addClass('modal-lg');
            modal.find('.modal-title').html('Cadastrar Cliente');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/addClient', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').click(function(event){
                $(this).off('click');
                registerClient(event);
            });
            break;
        case 'payment':
            client_id = $('#client_id').val();
            modal.find('.modal-dialog').addClass('modal-lg');
            modal.find('.modal-title').html(_lang['payment']);
            modal.find('.modal-body').load(wwwroot+'/terminal_async/paymentWindow?client_id=' + client_id, function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').hide();
            break;
        case 'details':
            modal.find('.modal-dialog').addClass('modal-lg');
            modal.find('.modal-title').html('Detalhes');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/orderLineDetails', data, function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').click(function(event){
                updateOrderLineDetails(event, $(this));
            });
            break;
        case 'order_details':
            modal.find('.modal-title').html('Detalhes da Compra');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/orderDetails', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').click(function(event){
                updateOrderDetails(event, $(this));
            });
            break;
        case 'park_order':
            modal.find('.modal-title').html('Park Order');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/parkOrder', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').unbind().click(function(event){
                parkOrder(event, $(this));
            });
            break;
        case 'search_parked_order':
            modal.find('.modal-dialog').addClass('modal-lg');
            modal.find('.modal-title').html('Search Parked Order');
            modal.find('.modal-body').load(wwwroot+'/terminal_async/searchParkedOrder', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').hide();
            break;
        case 'cashout':
            modal.find('.modal-title').html(_lang['cashout']);
            modal.find('.modal-body').load(wwwroot+'/terminal_async/moneyMovement?type=cashout', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').unbind().click(function(event){
                registerMoneyMovement(event, $(this));
            });
            break;
        case 'deposit':
            modal.find('.modal-title').html(_lang['deposit']);
            modal.find('.modal-body').load(wwwroot+'/terminal_async/moneyMovement?type=deposit', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').unbind().click(function(event){
                registerMoneyMovement(event, $(this));
            });
            break;
        case 'order_discount':
            modal.find('.modal-title').html(_lang['order_discount']);
            modal.find('.modal-body').load(wwwroot+'/terminal_async/orderDiscount', function(){
                modal.find(".modal-content").loading('stop');
            });
            modal.find('.modal-footer').show();
            modal.find('.modal-footer button[type=submit]').unbind().click(function(event){
                applyOrderDiscount(event, $(this));
            });
            break;
        default:
            break;
    }
}

/**
 * Fecha modal genérica
 */
function closeGenericModal() {
    var modal = $('#generic-modal');
    modal.find(".modal-content").loading('stop');
    modal.modal('hide');
}

function chooseEmployee(id, name) {
    closeGenericModal();
    $('body').loading();
    $('#employee_name').val(name.toUpperCase());
    $('#employee_id').val(id);
    var url = wwwroot+'/terminal_async/updateOrder';
    var params = {
        user_id: id
    };
    var posting = $.post(url, params);

    posting.done(function(data){
        $('body').loading('stop');
    }).fail(function(a, b, c){
        $('body').loading('stop');
        console.log(a, b, c);
    });
}

/**
 * Cria uma order vazia para ser usada na tela do terminal
 */
function initOrder() {
    pos_session_id = $('#pos_session_id').val();
    if (!pos_session_id) {
        window.location = wwwroot + '/terminal/open';
        return false;
    }
    $('#product_table > tbody').html('');
    var url = wwwroot+'/terminal_async/initOrder?pos_session_id=' + pos_session_id;

    $.ajax({
        url: url,
        dataType: 'json',
        success: function (data) {
            $('#employee_id').val(data.items.user_id);
            $('#employee_name').val(data.items.employee_name.toUpperCase());
            $('#client_id').val(data.items.person_id);
            $('#client_name').val(data.items.client_name.toUpperCase());
            // recupera orderlines
            if (data.extra != false) {
                for (i in data.extra) {
                    selectProduct(data.extra[i]['product_id'], data.extra[i]['size_color_id'], data.extra[i]['qty'], data.extra[i]['id']);
                }
            }
        },
        error: function (a, b, c) {
            console.log(a, b, c);
        }
    });
}

/*
 * Atualiza total da compra
 */
function updateTotal() {
    var url = wwwroot + '/terminal_async/updateOrderTotal';
    $.ajax({
        url: url,
        success: function(data) {
            total = parseFloat(data.items.amount);
            vat = parseFloat(data.items.vat);
            $('#total-output').text(currency+parseFloat(data.items.amount_outstanding).formatMoney(2, ',', '.'));
            $('#vat-output').text(currency+parseFloat(data.items.vat).formatMoney(2, ',', '.'));
            $('#vat-ex-output').text(currency+parseFloat(total-vat).formatMoney(2, ',', '.'));
        }
    });
}

function startPayment() {
    if ($('#employee_id').val() == '') {
        $.alert({
            type: 'red',
            title: 'Erro',
            content: _lang['message_0002'],
            theme: 'light'
        });
        return false;
    }
    if ($('#client_id').val() == '') {
        $.alert({
            type: 'red',
            title: 'Erro',
            content: 'Identifique o cliente para continuar',
            theme: 'light'
        });
        return false;
    }
    
    openGenericModal('payment');
}

/*
 * Seleciona linha de produto
 */
function checkProductRow(dis) {
    current_product_row = $(dis).attr('id');

    $('.product_row').removeClass('row-selected');
    $('#'+current_product_row).addClass('row-selected');
}

function chooseClient(id, name) {
    closeGenericModal();
    $('#client_id').val(id);
    $('#client_name').val(name.toUpperCase());

    $('body').loading();

    var url = wwwroot+'/terminal_async/updateOrder';
    var params = {
        person_id: id
    };
    var posting = $.post(url, params);

    posting.done(function(data){
        $('body').loading('stop');
    }).fail(function(a, b, c){
        $('body').loading('stop');
        console.log(a, b, c);
    });
}

function registerClient(e) {
    e.preventDefault();

    var form_data = $('#form-add-client').serializeArray();

    var url = wwwroot+'/terminal_async/registerClient';

    var posting = $.post(url, form_data);

    posting.done(function(data) {
        if (data.status.code == 200) {
            chooseClient(data.extra, data.items.name);
        }
    }).fail(function(a, b, c) {
        console.log(a, b, c);
    });
}

// check for existing gift card
function checkGiftCard() {
    // check for existing gift card
    var url = wwwroot+'/terminal_async/checkGiftCard?serial='+$('#order_line_serial').val()+'&order_line_id='+$('#order_line_id').val();
    $.ajax({
        url: url,
        method: 'GET',
        success: function(data) {
            //console.log(data);
            if (data.status.code == 200) {
                $.confirm({
                    title: 'Confirmation',
                    content: 'This gift card already exists. Please select another one.',
                    type: 'blue',
                    buttons: {
                        ok: {
                            text: 'OK',
                            keys: ['enter'],
                            action: function() {
                                removeLastProduct();
                            }
                        }
                    }
                });
            }
        },
        error: function(a, b, c) {
            console.log(a, b, c);
        }
    });
}

// TODO: recharge gift card
function rechargeGiftCard() {
    var form_data = $('#form-order-line').serializeArray();

    var url = wwwroot+'/terminal_async/rechargeGiftCard';

    var posting = $.post(url, form_data);

    posting.done(function(data){
        if (data.status.code == 200) {
            $.alert({
                type: 'green',
                title: _lang['success'],
                content: _lang['message_0001'],
                theme: 'light'
            });
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

function updateOrderLineDetails(e, dis) {
    dis.off('click');

    e.preventDefault();

    var form_data = $('#form-order-line').serializeArray();

    checkGiftCard();

    // update order line
    var url = wwwroot+'/terminal_async/updateOrderLineDetails';

    var posting = $.post(url, form_data);

    posting.done(function(data){
        if (data.status.code == 200) {
            $('.product_row').remove();
            initOrder();
            closeGenericModal();
            updateTotal();
            current_input = $('#product_input');
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

function removeLastProduct() {
    var current_product_row = $('#product_table > tbody > tr.product_row:last-child').attr('id');

    keypadFunction('delete');
}

function updateOrderDetails(e, dis) {
    dis.off('click');

    e.preventDefault();

    var form_data = $('#form-order').serializeArray();

    var url = wwwroot+'/terminal_async/updateOrder';

    var posting = $.post(url, form_data);

    posting.done(function(data){
        if (data.status.code == 200) {
            closeGenericModal();
            current_input = $('#product_input');
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

/*
 * Starts the parking order process.
 * Checks if the order description already exists and the order needs to be joined.
 */
function parkOrder(e, dis) {
    // dis.off('click');
    // dis.bind('click');

    e.preventDefault();

    var form_data = $('#park-order-form').serializeArray();

    // check if order identification already exists
    var url = wwwroot+'/terminal_async/parkOrder?verify=true';
    var posting = $.post(url, form_data);
    posting.done(function(data){
        if (data.status.code == 200) {
            $.confirm({
                title: 'Confirmation',
                content: 'This parked order already exists. Do you wish to join into it?',
                type: 'yellow',
                buttons: {
                    confirm: {
                        text: 'Join',
                        keys: ['enter'],
                        btnClass: 'btn-blue',
                        action: function() {
                            doParkOrder(form_data, data.items.id);
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        keys: ['esc']
                    }
                }
            });
        }
        else if (data.status.code == 404) {
            doParkOrder(form_data);
        }
    });
}

/*
 * Park orders into the database.
 * Receives optional order_id if the order needs to be joined
 */
function doParkOrder(form_data, order_id) {
    // actually park order
    var url = wwwroot+'/terminal_async/parkOrder';

    if (typeof order_id != 'undefined') {
        form_data.push({
            name: 'order_id',
            value: order_id
        });
    }

    var posting = $.post(url, form_data);

    posting.done(function(data){
        if (data.status.code == 200) {
            window.location = wwwroot + '/terminal/index?clear=true';
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

function cancelOrder() {
    $.confirm({
        title: 'Confirmação!',
        content: 'Tem certeza que deseja cancelar essa compra?',
        type: 'red',
        buttons: {
            confirm: {
                text: 'Cancelar Compra',
                keys: ['enter'],
                btnClass: 'btn-red',
                action: function() {
                    window.location = wwwroot + '/terminal/index?cancel=true'
                }
            },
            cancel: {
                text: 'Voltar',
                keys: ['esc'],
            }
        }
    });
}

var current_scroll_position = 0;
function scrollPanel(direction) {
    var container = $('#products_container');
    var limit = container.height();
    var pace = container.height() / 3;

    if (direction == 'up') {
        if (current_scroll_position <= 0) {
            return;
        }
        else {
            current_scroll_position -= pace;
        }
    }
    else if (direction == 'down') {
        current_scroll_position += pace;
    }

    if (current_scroll_position < 0) {
        current_scroll_position = 0;
    }
    else if (current_scroll_position > limit) {
        current_scroll_position = limit;
    }

    container.animate({
        scrollTop: current_scroll_position
    }, 300);
}

function useFavoriteButton(_this) {
    var btn = $(_this);
    // for now only set items using arcticle number
    var product_number = btn.data('command');

    var e = $.Event( "keydown", { keyCode: 13 } );
    $('#product_input').val(product_number).trigger(e);
}

function setModalTitle(title) {
    $('#generic-modal').find('.modal-title').html(title);
}

function printOrder() {
    return;
    var url = wwwroot + '/terminal_async/printOrder';
    $('body').loading();
    $.ajax({
        url: url,
        method: 'GET',
        timeout: 10000,
        success: function(data) {
            $('body').loading('stop');
            //console.log(data);
            if (data.status.code == 200) {
                $.alert({
                    type: 'green',
                    title: 'Sucesso',
                    content: 'Impressão enviada',
                    theme: 'light'
                });
            }
            else if (data.status.code == 601) {
                $.alert({
                    type: 'red',
                    title: 'Erro',
                    content: 'Servidor de impressão não cadastrado',
                    theme: 'light'
                });
            }
            else {
                $.alert({
                    type: 'red',
                    title: 'Erro',
                    content: 'Erro ao imprimir',
                    theme: 'light'
                });
            }
        },
        error: function(a, b, c) {
            $('body').loading('stop');
            console.log(a, b, c);
            if (b == 'timeout') {
                $.alert({
                    type: 'red',
                    title: 'Erro',
                    content: 'Servidor de impressão não encontrado',
                    theme: 'light'
                });
                return false;
            }
            else {
                $.alert({
                    type: 'red',
                    title: 'Erro',
                    content: 'Erro interno #9077',
                    theme: 'light'
                });
                return false;
            }
        }
    });
}

// for cashouts/deposits on the cashier
function registerMoneyMovement(e, dis) {
    dis.off('click');

    e.preventDefault();

    var form_data = $('#form-money-movement').serializeArray();

    var url = wwwroot+'/terminal_async/moneyMovement';

    var posting = $.post(url, form_data);

    posting.done(function(data){
        if (data.status.code == 200) {
            closeGenericModal();
            current_input = $('#product_input');
            $.alert({
                type: 'green',
                title: _lang['success'],
                content: _lang['all_done'],
                theme: 'light'
            });
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}

function applyOrderDiscount(e, dis) {
    dis.off('click');

    e.preventDefault();

    var form_data = $('#form-discount').serializeArray();

    var url = wwwroot+'/terminal_async/orderDiscount';

    var posting = $.post(url, form_data);

    posting.done(function(data){
        console.log(data);
        if (data.status.code == 200) {
            closeGenericModal();
            initOrder();
            updateTotal();
            current_input = $('#product_input');
        }
    }).fail(function(a, b, c){
        console.log(a, b, c);
    });
}


$(function(){
    initOrder();
    updateTotal();
    // procura produtos ao pressionar "enter"
    $('#product_input').keydown(function(e){
        if (e.keyCode == 13) {
            searchProducts();
        }
    });
    $('#products_container').scroll(function(){
        current_scroll_position = $(this).scrollTop();
    });
});