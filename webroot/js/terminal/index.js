(function() {
	console.log('Print stuff loaded');

    var beforePrint = function() {
        console.log('Functionality to run before printing.');
    };

    var afterPrint = function() {
        console.log('Functionality to run after printing');
    };

    if (window.frames['print-nota'].matchMedia) {
        var mediaQueryList = window.frames['print-nota'].matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;

}());
// format numbers to and from pritable format - R$ 12.345,67
function numberFormat(value, printable=false, prefix=false) {
	var ret = value;
	if (printable == true) {
		valuta_prefix = prefix == true ? 'R$ ' : '';
		value = parseFloat(value);
		ret = valuta_prefix+''+value.formatMoney(2, ',', '.');
	}
	else {
		value = ''+value;
		value = value.replace('R$ ', '');
		value = value.replace('.', '');
		value = value.replace(',', '.');
		ret = parseFloat(value);
	}
	return ret;
}

function select_user(user_id, user_name){ 
	localStorage.setItem('user_id', user_id);
	localStorage.setItem('user_name', user_name);

	if (!localStorage.getItem('order_id')) {
        var baseUrl = my_url+"Terminal/terminal/startOrder/"+user_id;

        $.ajax({
            url: baseUrl,
            async: false,
        }).done(function (data) {
        	console.log('Start order: ', data);
            localStorage.setItem('order_id', data.items);
        }).fail(function (jqXHR, textStatus, c) {
        	console.log('Error choosing user');
            console.log(jqXHR, textStatus, c);
        });
    }

    $(".display_user_name").val(user_name);
    $("#modal_choose_user").modal('hide');
}

function select_customer(customer_id, customer_name) {
	$(".display_customer_name").val(customer_name);
	localStorage.setItem('customer_id', customer_id);
	localStorage.setItem('customer_name', customer_name);
	$("#modal_choose_customer").modal('hide');
	
	return false;// for now
}

function cancelOrder(confirm) {
	var cancel = false;
	if (confirm == true) {
		swal({
			title: "Tem certeza?",
			text: "A compra será cancelada e todos os dados serão perdidos.",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				localStorage.clear();
				location.reload();
			}
		});
	}
	else {
		cancel = true;
	}

	if (cancel == true) {
		localStorage.clear();
		location.reload();
	}
}

function formatFloat(val) {
	var result = val+'';
	result = result.replace('.', '').replace(',', '.');
	return parseFloat(result);
}

// inserts a new product on the main product table
// receives product number
// returns product info
function getProductDetails(product_id) {
	// default amount
	qty = 1;
	// if amount is informed
	if (product_id.indexOf('*') > 0) {
		input_array = product_id.split('*');
		product_id = input_array[1];
		qty = input_array[0];
	}
	regex = /[0-9]{1,}/;
	// search by product name
	if (regex.test(product_id) == false) {
		$('#modal_choose_product').modal('show');
		$('#find_product_filter > label > input').val(product_id).focus();
	}
	// search by product number/id/barcode
	else {
		var url = my_url+"Terminal/terminal/searchProductsMainTable?product_id="+product_id;
		// gets product info
		$.ajax({
			url: url,
			dataType: 'json',
			success: function (data) {
				console.log({data});
				if (data.status.code == 200) {
					insertProductFromList(data.items[0], qty);
					setBtnLabelParked(false);
				}
				else if (data.status.code == 404) {
					swal('Error', 'Produto não encontrado', 'error');
				}
			},
			error: function(a, b, c) {
				console.log(a, b, c);
			}
		});
	}
}

// inserts a new row on product table, receives product array
function insertNewProductRow(product) {
	container = $('#product_table > tbody');

	tr = $('<tr></tr>').attr('id', 'product_'+product['productNumber']);
	td = [
		$('<td></td>').html(product['productNumber']),
		$('<td></td>').html(product['name']),
		$('<td></td>').html(numberFormat(product['qty'], true, false))
			.addClass('product_qty')
			.css('text-align', 'right'),
		$('<td></td>').html(numberFormat(product['price'], true, true))
			.addClass('product_unit_price')
			.css('text-align', 'right'),
		$('<td></td>').html(numberFormat(product['discount'], true, true))
			.addClass('product_discount')
			.css('text-align', 'right'),
		$('<td></td>').html(numberFormat(product['totalPrice'], true, true))
			.addClass('product_total_price')
			.css('text-align', 'right'),
		$('<td></td>').html(product['description'])
	];
	tr.append(td);
	container.append(tr);
}

// insert a new product on the main table from the product list table
// receives product array
function insertProductFromList(product, qty) {
	if (typeof qty == 'undefined') {
		qty = 1;
	}
	// get localStorage info about products on the table
	var product_list = JSON.parse(localStorage.getItem('product_list'));

	// increase amount and total price if the product already exists
	if (product['productNumber'] in product_list) {
		id = 'product_'+product['productNumber'];
		qty_current = numberFormat($('#'+id+' > td.product_qty').html());
		qty = parseInt(qty) + qty_current;
		price = numberFormat($('#'+id+' > td.product_unit_price').html());
		total_price = qty * price;
		$('#'+id+' > td.product_total_price').html(numberFormat(total_price, true, true));
		$('#'+id+' > td.product_qty').html(qty);

		product['qty'] = qty;
		product['totalPrice'] = total_price;
		product['discount'] = 0;
		product['print_receipt'] = true;
	}
	// inserts a new line
	else {
		if (product['sizeName']) {
			product['name'] = product['name'] + ' - ' + product['sizeName'];
			if (product['colorName']) {
				product['name'] = product['name'] + '/' + product['colorName'];
			}
		}
		product['qty'] = qty;
		product['totalPrice'] = parseFloat(product['price']) * parseFloat(qty);
		product['discount'] = 0;
		product['print_receipt'] = true;

		insertNewProductRow(product);
	}

	$('table#product_table tbody tr#product_'+product['productNumber']).trigger('click');

	product_list[product['productNumber']] = product;

	localStorage.setItem('product_list', JSON.stringify(product_list));

	$("#modal_choose_product").modal('hide');

	calculateTotal();
}

// retrieves product table from localStorage
function retrieveProductTable() {
	if (!localStorage.getItem('product_list')) {
		return false;
	}

	var product_list = JSON.parse(localStorage.getItem('product_list'));

	$('#product_table > tbody').html('');

	if (Object.size(product_list) > 0) {
		setBtnLabelParked(false);
		for (i in product_list) {
			insertNewProductRow(product_list[i]);
		}
	}
	else {
		setBtnLabelParked(true);
		return false;
	}
}

function setBtnLabelParked(get_order = false){
	if(get_order){
		$('#set_btn_label_parked').removeAttr('onclick');
		$('#set_btn_label_parked').html('Resgatar Compra');
		$('#set_btn_label_parked').attr('onclick', 'searchParkedOrders()');
	} else {
		$('#set_btn_label_parked').removeAttr('onclick');
		$('#set_btn_label_parked').html('Salvar Compra');
		$('#set_btn_label_parked').attr('onclick', 'parkOrder()');
	}
}

// get details for current selected product row
function getCurrentProductRow() {
	target_id = localStorage.getItem('selected_product_row');
	product_no = target_id.replace('product_', '');
	row_obj = $('#'+target_id);
	// return object
	return_obj = {
		'row_id': target_id,
		'row_id_global': 'table#product_table tbody tr#'+target_id,
		'row_obj': $('#'+target_id),
		'product_no': target_id.replace('product_', '')
	};
	return return_obj;
}

function getCurrentActiveElement() {
	var target_id = localStorage.getItem('active_element');
	// if (target_id == '' || $(target_id).length == 0) {
	// 	localStorage.setItem('active_element', 'input_product_no');
	// 	target_id = 'input_product_no';
	// }
	return target_id;
}

// uses numeric keypad to type into product input field and other fields
function keypadType(char) {
	var target_id = getCurrentActiveElement();
	var target = $('input#'+target_id);
	// if input text is selected, if erases it up before writing back in
	if (isTextSelected(target_id) == true) {
		target.val('');
	}
	// check if it's a number
	var match = char.match(/[0-9]/gi);
	if (match) {
		current = target.val();
		target.val(current+''+char);
	}
	// trigger enter
	else if (char == '=') {
		target.trigger({
		    type: 'keydown',
		    keyCode: 13
		});
	}
	// backspace
	else if (char == '<') {
		current = target.val();
		current = current.slice(0, -1);
		target.val(current);
	}
	else if (char == ',') {
		current = target.val();
		target.val(current+',');
	}
	else {// default chars that are not numbers
		current = target.val();
		target.val(current+''+char);
	}
	target.trigger('keydown');
}

// executes actions from function buttons
function keypadFunction(char) {
	var target_id = getCurrentActiveElement();
	switch (char) {
		case 'delete':
			target_id = localStorage.getItem('selected_product_row');
			// get product number and row id
			product_list = JSON.parse(localStorage.getItem('product_list'));
			// remove from product table
			$('#'+target_id).remove();
			product_no = target_id.replace('product_', '');
			// remove from localStorage
			delete product_list[product_no];
			localStorage.setItem('product_list', JSON.stringify(product_list));
			calculateTotal();
			if(Object.keys(product_list).length == 0){
				setBtnLabelParked(true);
			}
			break;
		case 'discount':
		case 'qty':
		case 'unit_price':
			enableInput(char);
			break;
		case 'details':
			// display modal
			$('#modal_order_line').modal('show');
			// get product info from list
			product_list = JSON.parse(localStorage.getItem('product_list'));
			target_id = localStorage.getItem('selected_product_row');
			product_no = target_id.replace('product_', '');
			product = product_list[product_no];
			// set product values on desired fields
			$('#order_line_product_name').html(product['name']);
			$('#order_line_product_no').val(product['productNumber']);
			if(product['print_receipt'] == true){
				$('#enable_print_receipt').prop('checked', true);
			} else {
				$('#enable_print_receipt').prop('checked', false);
			}
			break;
		default:
			break;
	}
}

function saveProductDetails() {
	var product_no = $('#order_line_product_no').val();
	var user_id = $('#order_line_user_id').val();
	if($('#enable_print_receipt').is(':checked')){
		var print_receipt = true;
	} else {
		var print_receipt = false;
	}
	// get product list
	var product_list = JSON.parse(localStorage.getItem('product_list'));
	product_list[product_no]['user_id'] = user_id;
	product_list[product_no]['print_receipt'] = print_receipt;
	localStorage.setItem('product_list', JSON.stringify(product_list));
	console.log(JSON.parse(localStorage.getItem('product_list')));
	$('#modal_order_line').modal('hide');
}

function disableInput() {
	var row = getCurrentProductRow();
	var product_list = JSON.parse(localStorage.getItem('product_list'));

	// get values
	var qty = product_list[row['product_no']]['qty'];
	var unit_price = product_list[row['product_no']]['price'];
	var discount = product_list[row['product_no']]['discount'];

	// calculates total price
	total_price = (qty * unit_price) - discount;

	// remove from dom
	$('#input_qty_'+row['row_id']+', #input_price_'+row['row_id']+', #input_discount_'+row['row_id']).remove();

	// puts back html values into the table
	row['row_obj'].find('td.product_qty').html(qty);
	row['row_obj'].find('td.product_discount').html(discount);
	row['row_obj'].find('td.product_unit_price').html(unit_price);
	row['row_obj'].find('td.product_total_price').html(total_price);
}

// enable dynamic inputs for product table
function enableInput(field) {
	disableInput();
	var row = getCurrentProductRow();
	var target_id = row['row_id_global']+' td.product_'+field;
	var current_value = numberFormat($(target_id).html(), false);
	var input_id = 'input_'+field+'_'+row['product_no'];
	// if the field already exists
	if ($('#'+input_id).length) {
		$('#'+input_id).focus();
		$('#'+input_id).trigger({
		    type: 'keydown',
		    keyCode: 13
		});
	}
	input = $('<input>')
		.attr({
			'type': 'text',
			'id': input_id,
			'value': ''//current_value
		})
		.addClass('table-input')
		.addClass('input-product-'+field);
	$(target_id).html(input);
	input.focus();
}

// alter row values and calculate total price again
function alterProductRow(row_id, input) {
	var row = $('#'+row_id);
	var product_no = row_id.replace('product_', '');

	// get values
	// parse float only cause they been thru numberFormat just before this step
	var qty = parseFloat(row.find('td.product_qty').html());
	var unit_price = parseFloat(row.find('td.product_unit_price').html());
	var discount = parseFloat(row.find('td.product_discount').html());

	// changes quantity
	if (input.hasClass('input-product-qty')) {
		qty = numberFormat(input.val());
	}
	else if (input.hasClass('input-product-discount')) {
		discount = numberFormat(input.val());
	}
	else if (input.hasClass('input-product-unit_price')) {
		unit_price = numberFormat(input.val());
	}
	
	// calculates total price
	total_price = (qty * unit_price) - discount;

	// removes input from DOM
	input.remove();

	// puts back html values into the table
	console.log('qty: ',qty);
	console.log('unit price: ',unit_price);
	row.find('td.product_qty').html(numberFormat(qty, true, false));
	row.find('td.product_discount').html(numberFormat(discount, true, true));
	row.find('td.product_unit_price').html(numberFormat(unit_price, true, true));
	row.find('td.product_total_price').html(numberFormat(total_price, true, true));

	// replace product data into localStorage
	product_list = JSON.parse(localStorage.getItem('product_list'));
	product_list[product_no]['price'] = unit_price;
	product_list[product_no]['qty'] = qty;
	product_list[product_no]['discount'] = discount;
	product_list[product_no]['totalPrice'] = total_price;

	localStorage.setItem('product_list', JSON.stringify(product_list));

	calculateTotal();
}

function calculateTotal() {
	var product_list = JSON.parse(localStorage.getItem('product_list'));
	// console.log(product_list);
	var total = 0.0;
	for (i in product_list) {
		if (!product_list[i]['totalPrice']) {
			continue;
		}
		total += parseFloat(product_list[i]['totalPrice']);
		
	}
	localStorage.setItem('total', total);
	// console.log(total, total.formatMoney(2, ',', '.'));
	$("#total").html(total.formatMoney(2, ',', '.'));
}

var current_scroll_position = 0;
function scrollPanel(direction) {
	var container = $('#products_container');
	var limit = container.height() + 100;
	var pace = container.height() / 2;

	if (direction == 'up') {
		if (current_scroll_position <= 0) {
			return;
		}
		else {
			current_scroll_position -= pace;
		}
	}
	else if (direction == 'down') {
		current_scroll_position += pace;
	}

	if (current_scroll_position < 0) {
		current_scroll_position = 0;
	}
	else if (current_scroll_position > limit) {
		current_scroll_position = limit;
	}

	container.animate({
        scrollTop: current_scroll_position
    }, 300);
}

function startPayment() {
	// $('#modal_payment').modal('show');
	$('#products-pane').slideUp();
	$('#payment-pane').slideDown();
	$('#payment-button').hide();
	if (parseFloat(localStorage.getItem('total')) < 0) {
		$('.payment-method-button').hide();
	}
	else {
		$('.payment-method-button').show();
	}
	
	var total = parseFloat(localStorage.getItem('total'));
	$('#payment_total').html(total.formatMoney(2, ',', '.'));
	calculatePaidRemaining();

	if (localStorage.getItem('total') < 0) {
		$('#refundNote').show();
	}
	else {
		$('#refundNote').hide();
	}
}

function stopPayment() {
	$('#products-pane').slideDown();
	$('#payment-pane').slideUp();
	$('#payment-button').show();
}

// update order and create ordelines with current products
function pay() {
    
	if (localStorage.getItem('remaining') > 0 || localStorage.getItem('total') == 0) {
		swal('Atenção!', 'A compra ainda possui valor em aberto.', 'warning');
		return false;
	}
	$('.loader').show();
	var product_list = JSON.parse(localStorage.getItem('product_list'));
	// console.log(product_list);
	if ((typeof product_list != 'object') || (product_list === null)) {
		alert('Could not register payment.');
		return;
	}

	// get form
	var form = $('form#payment-form');
	// get payment methods
	var form_data = form.serializeArray();
	var payment_methods = {};
	var refund_notes = {};
        var countpay = 0;
	$.each(form_data, function(i, field){
		id = field.name;
		console.log(id);
		if (id.indexOf('payment_method_value_') >= 0) {
			id = id.replace('payment_method_value_','');
            countpay = parseInt(countpay) + parseInt(id);
			payment_methods[id] = field.value;
		}
		else if (id.indexOf('refund_note_') >= 0) {
			id = id.replace('refund_note_', '');
			refund_notes[id] = field.value;
		}
	});

	// atualiza arquivos viacliente.txt e vialoja.txt
	var url = my_url+'Terminal/terminal/updatePrintFiles';
	var data = {
		viacliente: localStorage.getItem('viacliente'),
		vialoja: localStorage.getItem('vialoja')
	}

	var posting = $.post(url, data);

	posting.done(function(data){
		console.log({data});
	});
        
	var url = my_url+'Terminal/terminal/pay';
	var data = {
		payment_methods: payment_methods,
		products: product_list,
		order_id: localStorage.getItem('order_id'),
		customer_id: localStorage.getItem('customer_id'),
		user_id: localStorage.getItem('user_id'),
                cartao: localStorage.getItem('cartao'),
                viacliente: localStorage.getItem('viacliente'),
		vialoja: localStorage.getItem('vialoja'),
		refund_note: 0
	};

	if (Object.size(refund_notes) > 0) {
		data['refund_note_list'] = refund_notes;
	}

	console.log(payment_methods, localStorage);
    
    // pagamento com dinheiro
	if (countpay == 1) {
		var posting = $.post(url, data);
		console.log(posting);

		posting.done(function(data) {
			console.log(data);
			$(".loader").fadeOut("slow");

			if (data.status.code == 200 || data.status == 200) {
				printReceipt(localStorage.getItem('order_id'), true, false, localStorage.getItem('product_list'));
			}
		});

		posting.fail(function(a, b, c){
			$(".loader").fadeOut("slow");
			console.log(a, b, c);
		});
	}
	// pagamento com cartão
	else {    
		/**
		* post nodejs
		*/
		var ajax = new XMLHttpRequest();

		ajax.open("POST", "minha-url-api", true);
		ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		ajax.send("email=teste@teste.com");

		ajax.onreadystatechange = function() {
			if (ajax.readyState == 4 && ajax.status == 200) {
				var dataapi = ajax.responseText;
				console.log(dataapi);
				return;
			}
		}

		// alert("teste dev");
		// return;
		var posting = $.post(url, data);
		console.log(posting);

		posting.done(function(data) {
			console.log(data);
			$(".loader").fadeOut("slow");

			if (data.status.code == 200 || data.status == 200) {
				printReceipt(localStorage.getItem('order_id'), true, true, localStorage.getItem('product_list'));
			}
		});

		posting.fail(function(a, b, c){
			$(".loader").fadeOut("slow");
			console.log(a, b, c);
		});
	}
}

function addPaymentMethodRow(id, name, refund_amount) {
	// refund note
	if (typeof refund_amount != 'undefined') {
		var remaining = refund_amount;
		var readOnly = true;
		var field_name = 'refund_note_'+id;
		var input_id = '';// no id so the virtual keyboard can't change the value
	}
	// regular payment method
	else {
		var remaining = calculatePaidRemaining();
		var readOnly = false;
		var field_name = 'payment_method_value_'+id;
		var input_id = 'payment-item-'+id+'-'+Math.floor((Math.random() * 5000) + 10);
	}
	var container = $('#table-payment-list > tbody');
	var tr_id = 'payment-row-'+Math.floor((Math.random() * 5000) + 10);
	var tr = $('<tr></tr>').attr('id', tr_id);
	var payment_method_input = $('<input>').attr({
		'type': 'text',
		'name': field_name,
		'id': input_id,
		'value': remaining
	}).addClass('table-input payment-row-input').css('width', '90px').prop('readOnly', readOnly);

	var td = [
		$('<td></td>').html(name),
		$('<td></td>').html(payment_method_input),
		$('<td></td>')
			.css('width', '50px')
			.html('<button type="button" class="btn btn-danger btn-xs" onclick="removePaymentMethodRow(\''+tr_id+'\', \''+id+'\');"><i class="fas fa-times"></i></button>')
	];
	tr.append(td);
	container.append(tr);
	if (readOnly == false) {
		payment_method_input.focus();
		document.getElementById(input_id).select();
		localStorage.setItem('active_element', input_id);
	}
	calculatePaidRemaining();
}

function removePaymentMethodRow(row_id, id) {
	$('#'+row_id).remove();
	calculatePaidRemaining();
}

function calculatePaidRemaining() {
	var total = parseFloat(localStorage.getItem('total'));
	var paid = 0.0;
	$('input.payment-row-input').each(function(index){
		value = ($(this).val() == '') ? 0.0 : parseFloat($(this).val());
		// console.log($(this).val(), value);
		paid += value;
	});
	var remaining = total - paid;
	localStorage.setItem('remaining', remaining);
	$('#payment_remaining').html(remaining.formatMoney(2, ',', '.'));
       
	return remaining;
}

// generates refund note for future use
// DEPRECATED
function saveRefundNote() {
	$('.loader').show();

	var product_list = JSON.parse(localStorage.getItem('product_list'));
	// console.log(product_list);
	if ((typeof product_list != 'object') || (product_list === null)) {
		alert('Could not register payment.');
		return;
	}

	var url = my_url+'Terminal/terminal/pay';
	var data = {
		products: product_list,
		order_id: localStorage.getItem('order_id'),
		customer_id: localStorage.getItem('customer_id'),
		user_id: localStorage.getItem('user_id'),
		refund_note: 1
	};

	var posting = $.post(url, data);
	console.log(posting);

	posting.done(function(data) {
		console.log(data);
		$(".loader").fadeOut("slow");
		if (data.status.code == 200) {
			// updates iframe url
			// src = $('#print-frame').attr('src');
			// src_new = src.substring(0, src.indexOf('='))+'='+localStorage.getItem('order_id');

			src = my_url+'terminal/terminal/printRefundNote?order_id='+localStorage.getItem('order_id');
			$('#print-frame').attr('src', src);
			// waits a little bit to make sure the frame is ready to print
			window.setTimeout(function() {
				window.frames["print-frame"].focus();
				window.frames["print-frame"].print();
				$(".loader").fadeOut("slow");
				swal({title: 'Sucesso!', text: 'Compra efetuada com sucesso!', type: 'success'})
				.then((value) => {
					cancelOrder();
				});

			}, 1200);
		}
	});

	posting.fail(function(a, b, c){
		$(".loader").fadeOut("slow");
		console.log(a, b, c);
	});
}

// print purchase receipt (not nota fiscal) for a certain order
function printReceipt(order_id, refresh, credit_card, ignore_products = false) {
	swal({title: 'Sucesso!', text: 'Compra efetuada com sucesso!', type: 'success'})
	.then((value) => {
		cancelOrder();
	});
	return false;

	console.log('Print receipt');
	if(ignore_products){
		var ignore_articles_no = [];
		var ignore_products = JSON.parse(ignore_products);
		for(var key in ignore_products){
			if(!ignore_products[key].print_receipt){
				ignore_articles_no.push(key);
			}
		}
		var ignore = ignore_articles_no.join();
		// alert(order_id+' - '+ignore);
		// return;
	}

	$.LoadingOverlay('show', {
		text: 'Gerando nota fiscal, aguardando resposta da SEFAZ...',
		textResizeFactor: 0.2
	});

	// generate nota
	var print_nota = document.getElementById('print-nota');
	print_nota.addEventListener("load", function() {
		window.frames["print-nota"].focus();
		window.frames["print-nota"].print();
		$.LoadingOverlay('hide');

		$('#print-frame').attr('src', my_url+'terminal/terminal/printReceipt?order_id='+order_id+'&ignore_products='+ignore);
		window.setTimeout(function() {
			window.frames["print-frame"].focus();
			window.frames["print-frame"].print();
		}, 1500);
		console.log('print-frame: ', my_url+'terminal/terminal/printReceipt?order_id='+order_id+'&ignore_products='+ignore);

		if (credit_card == true) {
			$('#print-viacliente').attr('src', my_url+'terminal/terminal/printAnything?type=viacliente');
			window.setTimeout(function() {
				window.frames["print-viacliente"].focus();
				window.frames["print-viacliente"].print();
			}, 2500);

			$('#print-vialoja').attr('src', my_url+'terminal/terminal/printAnything?type=vialoja');
			window.setTimeout(function() {
				window.frames["print-vialoja"].focus();
				window.frames["print-vialoja"].print();
			}, 3500);
		}

		if (refresh == true) {
			swal({title: 'Sucesso!', text: 'Compra efetuada com sucesso!', type: 'success'})
			.then((value) => {
				cancelOrder();
			});
		}
	});
	src = my_url+'terminal/terminal/generateNotaFiscal/'+order_id;
	print_nota.src = src;

	// src = my_url+'terminal/terminal/printReceipt?order_id='+order_id;
	// $('#print-frame').attr('src', src);
	// // waits a little bit to make sure the frame is ready to print
	// window.setTimeout(function() {
	// 	window.frames["print-frame"].focus();
	// 	window.frames["print-frame"].print();

	// 	$(".loader").fadeOut("slow");
	// 	if (refresh == true) {
	// 		swal({title: 'Sucesso!', text: 'Compra efetuada com sucesso!', type: 'success'})
	// 		.then((value) => {
	// 			cancelOrder();
	// 		});
	// 	}

	// }, 500);
}

// uses an existing refund note to apply discount to the purchase
function useRefundNote() {
	// if (localStorage.getItem('refund_note_id') != null) {
	// 	swal('Atenção!', 'Apenas um retorno por compra.', 'warning');
	// 	return;
	// }
	// else {}
	swal({
		text: "Informe o ID do desconto",
		content: "input"
	}).then((value) => {
		var url = my_url+"Terminal/terminal/getOrder/"+value+'/1';// second paramenter to force json return
		$.ajax({
			url: url,
			method: 'GET',
			success: function(data) {
				console.log(data);
				// TODO: insert payment method line with order amount
				addPaymentMethodRow(data[0]['idorder'], 'Refund', data[1]);
			}
		});
	});
}

// check if text is selected inside an input. returns boolean
function isTextSelected(input_id) {
	input = document.getElementById(input_id);
    if (typeof input.selectionStart == "number") {
        return input.selectionStart == 0 && input.selectionEnd == input.value.length;
    } else if (typeof document.selection != "undefined") {
        return document.selection.createRange().text == input.value;
    }
}

jQuery.expr[':'].focus = function( elem ) {
	return elem === document.activeElement && ( elem.type || elem.href );
};

function parkOrder() {
	var order_id = localStorage.getItem('order_id');
	var product_list = JSON.parse(localStorage.getItem('product_list'));
	console.log(product_list);
	if ((typeof product_list != 'object') || (product_list === null) || (Object.size(product_list) == 0)) {
		swal('Erro', 'Nenhum produto para registrar', 'error');
		return;
	}

	swal({
		title: 'Informe identificação da compra',
		content: {
			element: "input",
			attributes: {
				value: localStorage.getItem('order_name'),
				type: "text",
			},
		},
	}).then((info) => {
		var url = my_url+'Terminal/terminal/pay';
		var data = {
			products: product_list,
			order_id: localStorage.getItem('order_id'),
			customer_id: localStorage.getItem('customer_id'),
			user_id: localStorage.getItem('user_id'),
			park_order: 1,
			extra_info: info
		};
		$.ajax({
			url: url,
			method: 'POST',
			data: data,
			success: function(data) {
				console.log(data);
				if (data.status.code == 200) {
					swal('Sucesso', 'Ordem salva com sucesso', 'success')
						.then((value) => {
							cancelOrder();
						});
				}
			},
			error: function(a, b, c) {
				console.log(a, b, c);
			}
		});
	});
}

function loadParkedOrder(order_id, order_name) {
	// populates localstorage
	var url = my_url+'Terminal/terminal/getOrderLines/'+order_id
	$.ajax({
		url: url,
		method: 'GET',
		success: function(data) {
			localStorage.setItem('order_id', order_id);
			localStorage.setItem('order_name', order_name);
			clearLocalStorage();
			console.log('loadParkedOrder: ', data);
			result = JSON.parse(data);
			console.log('loadParkedOrder: ', result);
			items = result.data;
			items_obj = {};
			for (i in items) {
				items_obj[items[i]['product_number']] = {
					idproduct: items[i]['product_id'],
					productNumber: items[i]['product_number'],
					name: items[i]['description'],
					price: parseFloat(items[i]['price']),
					description: '',
					qty: items[i]['qty'],
					print_receipt: true,
					totalPrice: parseFloat(items[i]['amount']),
					discount: parseFloat(items[i]['discount'])
				}
			}
			localStorage.setItem('product_list', JSON.stringify(items_obj));
			retrieveProductTable();
			calculateTotal();
			$('#modal_choose_order').modal('hide');
		},
		error: function(a, b, c) {
			console.log(a, b, c);
		}
	});
}

function clearLocalStorage() {
	localStorage.setItem('product_list', JSON.stringify({}));
	localStorage.setItem('payment_list', JSON.stringify({}));
	localStorage.setItem('total', 0.0);
	localStorage.setItem('amount_paid', 0.0);
	localStorage.setItem('amount_outstanding', 0.0);
	localStorage.setItem('cash_back', 0.0);
}

// apply discount to the whole order
function applyGeneralDiscount() {
	swal({
		title: 'Informe o valor do desconto (R$):',
		content: 'input'
	}).then((amount) => {
		if (amount > 0 && typeof amount != 'undefined' && amount != '') {
			var product_list = JSON.parse(localStorage.getItem('product_list'));
			var size = Object.size(product_list);
			var discount = parseFloat(amount) / size;
			for (i in product_list) {
				product_list[i]['discount'] = discount;
				product_list[i]['totalPrice'] = (parseFloat(product_list[i]['price']) * parseFloat(product_list[i]['qty'])) - discount;
			}

			localStorage.setItem('product_list', JSON.stringify(product_list));
			retrieveProductTable();
			calculateTotal();
		}
	});
}

// close pos session
function closePosSession() {
	openPane('pos-pane');
	console.log('close session');
}

function openPane(pane_id) {
	$('.system-pane').hide();
	$('#'+pane_id).slideDown();
}

function testPrint() {
	$('#print-viacliente').attr('src', my_url+'terminal/terminal/printAnything?type=viacliente');
	window.setTimeout(function() {
		window.frames["print-viacliente"].focus();
		window.frames["print-viacliente"].print();
	}, 500);
	
	$('#print-vialoja').attr('src', my_url+'terminal/terminal/printAnything?type=vialoja');
	window.setTimeout(function() {
		window.frames["print-vialoja"].focus();
		window.frames["print-vialoja"].print();
	}, 500);
}

function searchParkedOrders(){
    $("#modal_choose_order").modal({show : true});
}

$(document).ready(function(){
	console.log('build: 1.28.8');
	// barcode detection
    // $(document).keydown(function(e) {
    // 	if ($('input').is(':focus')) {
    // 		return;
    // 	}
    // 	// var code = (e.keyCode ? e.keyCode : e.which);
    // 	$('#input_product_no').focus();
    // });

    localStorage.setItem('viacliente', 'Teste de via cliente');
    localStorage.setItem('vialoja', 'Teste de via loja');

	console.log(localStorage);

	// check if user was already picked to avoid asking every time
	if (!localStorage.getItem('user_id')) {
		$("#modal_choose_user").modal({show : true});
	}
	else {
		select_user(localStorage.getItem('user_id'), localStorage.getItem('user_name'));
	}

	localStorage.setItem('active_element', 'input_product_no');
	localStorage.removeItem('refund_note_id');
	openPane('products-pane');
	// $('#payment-pane, #pos-pane').hide();

	retrieveProductTable();
	calculateTotal();

	$(document).on('focus', 'input', function(){
		localStorage.setItem('active_element', this.id);
	});

	$('.btn').click(function(){
    	$(this).blur();
    	$('#'+localStorage.getItem('active_element')).focus();
    });

	// open keyboard for proper fields
	// $('#keyboard').mlKeyboard({
	// 	layout: 'en_US',
	// 	trigger: '#open-keyboard'
	// });

	// selects default customer
	select_customer(0, 'Walk in customer');

	// TODO: insert product from input field using product number directly
	$('#input_product_no').keydown(function(event) {
		// enter pressed
		if (event.keyCode == 13) {
			var product_no = $(this).val();
			getProductDetails(product_no);
			$(this).val('');
		}
	});

	// creates empty product list
	if (!localStorage.getItem('product_list')) {
		clearLocalStorage();
	}

	// clears localStorage items that need to be cleared
	if (localStorage.getItem('selected_product_row')) {
		localStorage.removeItem('selected_product_row');
	}

	/*
	 * CHOOSE USER
	 */
	// open user modal
	$(".change_user").click(function(){
        $("#modal_choose_user").modal({show : true});
    });

	// close modal dialog whenever used (serves all dialogs)
    $(".close-dialog").click(function(){
        $(".modal-dialog").hide();
    });

    /*
	 * CHOOSE CUSTOMERS
	 */
    // open customer modal
    $(".change_customer").click(function(){
        if($(".display_user_name").val() == "") {
            $("#modal_choose_user").modal({show : true});
        }
        else {
            $("#modal_choose_customer").modal({show : true});
        }
    });
    // table to search customers
	var table_find_customer = $('#find_customer').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": my_url+"Terminal/terminal/searchCustomers"
	});
	// onclick for table customers
	$('#find_customer tbody').on( 'click', 'tr', function () {
	    var row = table_find_customer.row(this).data();
	    select_customer(row[0], row[1]);
	} );

    /*
	 * SEARCH PRODUCTS
	 */
	 // table to search products
	var table_find_product = $('#find_product').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": my_url+"Terminal/terminal/searchProducts",
		"pagingType": "full_numbers",
		"paging": true,
		"lengthMenu": [10, 25, 50, 75, 100],
	});
    // open product modal
    $('#add_product').click(function(){
    	$('#modal_choose_product').modal('show');
    	$('#find_product_filter > label > input').val('').focus();
    	table_find_product.search('').draw();
    });
	// onclick for table products
	$('#find_product tbody').on( 'click', 'tr', function () {
	    var row = table_find_product.row(this).data();
	    console.log({row});
	    getProductDetails(row[0], row[4]);
	} );

	/*
	 * PARKED ORDERS
	 */
	// open orders modal
	$(".search_parked_orders").click(function(){
        $("#modal_choose_order").modal({show : true});
    });
	// table orders
	var table_find_order = $('#find_order').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": my_url+"Terminal/terminal/searchParkedOrders"
	});

	// onclick for table orders
	$('#find_order tbody').on( 'click', 'tr', function () {
	    var row = table_find_order.row(this).data();
	    console.log(row);
	    loadParkedOrder(row[0], row[1]);
	});

	/*
	 * MAIN PRODUCT TABLE
	 */
	// what happens when you click in a row in the main products list
	$(document).on("click", "table#product_table tbody tr", function(e) {
	    // check if row is already selected
	    if (localStorage.getItem('selected_product_row') != this.id) {
	    	$("table#product_table tbody tr").each(function(index){
	    		$(this).removeClass('selected');
	    	});
	    	$(this).addClass('selected');
	    	localStorage.setItem('selected_product_row', this.id);
	    }
	});

	// change qty, price or discount on a row
	$(document).on('keydown', 'table#product_table tbody tr td input.input-product-qty, table#product_table tbody tr td input.input-product-unit_price, table#product_table tbody tr td input.input-product-discount', function(event) {
		console.log(event.keyCode);
		if (event.keyCode == 13) {// enter
			row_id = $(this).parent().parent().attr('id');
			alterProductRow(row_id, $(this));
		}
		else if (event.keyCode == 27) {// esc
			disableInput();
		}
	});

	// read and sum the payment rows
	$(document).on('keyup', 'table#table-payment-list tbody tr td input.payment-row-input', function(event){
		// console.log('keydown', $(this).val());
		calculatePaidRemaining();
	});
});

