/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    var counts = [0];
    var resizeOpts = {
        handles: "all", autoHide: true
    };

    $('#cp8').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });

    $('#cp9').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });

    $(".dragImg").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $(".button-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $(".table-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $(".image-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $(".customer-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });
    
    $(".employee-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });
    
    $(".order-info-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });
    
    $(".last-sale-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });
    
    $(".tabs-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $("#dropHere").droppable({
        greedy: true,
        drop: function (event, ui) {
            if (ui.draggable.hasClass("dragImg")) {
                $(this).append($(ui.helper).clone());
                //Pointing to the dragImg class in dropHere and add new class.
                $("#dropHere .dragImg").addClass("item-" + counts[0]);
                $("#dropHere .img").addClass("imgSize-" + counts[0]);

                //Remove o item para poder arrastar-lo novamente
                $("#dropHere .item-" + counts[0]).removeClass("dragImg ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable(resizeOpts);
            }
            
            if (ui.draggable.hasClass("button-widget")) {
                $(this).append('<div \n\
                class="ui-draggable-handle item-'+counts[0]+' imgSize-'+counts[0]+' btn btn-primary ui-draggable ui-resizable" \n\
                style="width:92px; height:43px" data-edit="false" data-item-id="'+counts[0]+'"><p style="margin: 0px">Button</p></div>');
                
                $("#dropHere .item-" + counts[0]).removeClass("button-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    resetForm('#form_info_btn');
                    var idElem = $(this).attr("data-item-id");
                    var data = $(".item-"+idElem)[0].dataset;
                    if(data['edit'] == 'true'){
                        editBtn(idElem, data);
                    }
                    var setID = $("#form_info_btn #idElemBtn").val(idElem);
                    var setRemove = $('#btn-remove').attr("onclick", 'removeElem('+idElem+')');
                    $('#myModal').modal({show : true});
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    snap: true,
                    containment: "#dropHere",
                    minHeight: 42,
                    minWidth: 50
                });
            }

            if (ui.draggable.hasClass("image-widget")) {
                $(this).append("<div id='image-widget' style='width: 150px; height:150px; background-color: #ccc;' \n\
                class='image-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <input id='inputFileToLoad' type='file' style='display: none' />\n\
                <p class='text-center' style='font-size: 16px;padding-top: 30px;'><span class='lnr lnr-picture'></span><br>Upload image</p></div>");

                $("#dropHere .item-" + counts[0]).removeClass("image-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $("#dropHere .item-" + counts[0]).addClass('current-edit-image');
                    $("#dropHere .item-" + counts[0]).find('#inputFileToLoad').click();
                    encodeImageFileAsURL();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 150,
                    minWidth: 150
                });
            }
            
            if (ui.draggable.hasClass("table-widget")) {
                $(this).append("<div id='table-widget' class='table-widget bordered-ui ui-draggable ui-draggable-dragging \n\
                item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"' style='background-color: rgb(237, 236, 236); width:571px; height:271px'>\n\
                <div style='width: 100%;'>\n\
                    <div class='input-group'>\n\
                        <input type='text' class='form-control insert_barcode' placeholder='Search for...'>\n\
                        <span class='input-group-btn'>\n\
                            <button class='btn btn-default new_product' type='button'>Add</button>\n\
                        </span>\n\
                    </div>\n\
                </div>\n\
                <table id='color' class='table-bordered pos_table' cellspacing='0' width='100%' style='margin: 0px;'>\n\
                    <thead>\n\
                    <tr>\n\
                        <th class='text-center' style='width: 10px'><input type='checkbox' name='all' onclick='markAll('color')'></th>\n\
                        <th style='width: 10px'>ID</th>\n\
                        <th>Product No</th>\n\
                        <th>Description</th>\n\
                        <th>Qty</th>\n\
                        <th>Un Price</th>\n\
                        <th>Discount</th>\n\
                        <th>Price</th>\n\
                        <th>Info</th>\n\
                    </tr>\n\
                    </thead>\n\
                <tbody>\n\
                    <tr class='new_product'>\n\
                        <td></td>\n\
                        <td></td>\n\
                        <td class='product_no'></td>\n\
                        <td class='product_description'></td>\n\
                        <td></td>\n\
                        <td class='product_price'></td>\n\
                        <td></td>\n\
                        <td></td>\n\
                        <td></td>\n\
                    </tr>\n\
                </tbody>\n\
                </table></div>");

                $("#dropHere .item-" + counts[0]).removeClass("table-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 217,
                    minWidth: 571
                });
            }
            
            if (ui.draggable.hasClass("customer-widget")){
                $(this).append("<div id='customer-widget' style='width:164px; height: 96px' \n\
                class='customer-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <input type='text' readonly='' placeholder='Customer' class='form-control display_customer_name'><br>\n\
                <input type='button' class='btn btn-primary change_customer' value='Change'>\n\
                </div>");

                $("#dropHere .item-" + counts[0]).removeClass("customer-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 96,
                    minWidth: 164
                });
            }
            
            if (ui.draggable.hasClass("employee-widget")){
                $(this).append("<div id='employee-widget' style='width:164px; height:96px' \n\
                class='customer-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <input type='text' readonly='' placeholder='Employee' class='form-control display_user_name'><br>\n\
                <input type='button' class='btn btn-primary change_user' value='Change'>\n\
                </div>");

                $("#dropHere .item-" + counts[0]).removeClass("customer-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 96,
                    minWidth: 164
                });
            }
            
            if (ui.draggable.hasClass("tabs-widget")){
                $(this).append("<div id='tabs-widget' style='width:236px' \n\
                class='tabs-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <ul class='nav nav-tabs'>\n\
                    <li class='active'><a data-toggle='tab' href='#favorite' aria-expanded='true'>Favorite</a></li>\n\
                    <li><a data-toggle='tab' href='#extraFunctions' aria-expanded='true'>Extra Functions</a></li>\n\
                    <li class='pull-right'><a onclick='openTabs("+counts[0]+")'>+</a></li>\n\
                </ul>\n\
                <div class='tab-content' data-tab-id='"+counts[0]+"'>\n\
                    <div id='favorite' class='tab-pane fade in active'>\n\
                      <div style='height:150px;'>\n\
                        <div class='btn btn-primary button_get_parked_order' style='margin: 5px;'>Get Parked <br>Order</div>\n\
                        <div class='btn btn-primary button_extra' style='margin: 5px;'>Extra <br>functions</div>\n\
                        <div class='btn btn-primary button_discount' style='margin: 5px;'>Discount</div>\n\
                        <div class='btn btn-primary button_cancel_order' style='margin: 5px;'>Cancel Order</div>\n\
                        <div class='btn btn-primary button_line_info' style='margin: 5px;'>Line info</div>\n\
                        <div class='btn btn-primary button_delete_line' style='margin: 5px;'>Delete line</div>\n\
                        <div class='btn btn-primary button_order_info' style='margin: 5px;'>Order info</div>\n\
                        <div class='btn btn-primary button_order' style='margin: 5px;'>Open Credit <br>Order</div>\n\
                        <div class='btn btn-primary button_pay' style='margin: 5px;'>Pay</div>\n\
                        <div class='btn btn-primary button_return' style='margin: 5px;'>Return</div>\n\
                        <div class='btn btn-primary button_kvittering' style='margin: 5px;'>Kvittering</div>\n\
                        <div class='btn btn-primary button_gavekort' style='margin: 5px;'>Lag Nytt <br>Gavekort</div>\n\
                      </div>\n\
                    </div>\n\
                    <div id='extraFunctions' class='tab-pane'>\n\
                      <div style='height:150px;'>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>With-drawal / Deposit</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Open Cash Drawer</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Print Receipt Copy</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Cash Statistics</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Credit Last Sale</div>\n\
                        <div class='btn btn-primary button_print_temp_receipt' style='margin: 5px;'>Print Temp. Receipt</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Settle Invoice By Cash</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Pre Payment</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Split Order</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Check Giftcard Status</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Check Electr. Gift Card</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Customer Orders</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Customer Statistics</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Customer Updating</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Article Statistics</div>\n\
                        <div class='btn btn-primary' style='margin: 5px;'>Article Updating</div>\n\
                      </div>\n\
                    </div>\n\
                  </div>\n\
                </div>");

                $("#dropHere .item-" + counts[0]).removeClass("tabs-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    openInfo(this);
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable(resizeOpts);
            }
            
            if (ui.draggable.hasClass("order-info-widget")){
                $(this).append("<div id='order-info-widget' style='width:164px; height: 96px; background-color: #EDECEC; border-radius: 5px' \n\
                class='order-info-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <h4>Order</h4><p style='padding-left:15px'>Total: <span class='total_order'>0.00</span></p>\n\
                </div>");

                $("#dropHere .item-" + counts[0]).removeClass("order-info-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 96,
                    minWidth: 164
                });
            }
            
            if (ui.draggable.hasClass("last-sale-widget")){
                $(this).append("<div id='last-sale-widget' style='width:164px; height: 96px; background-color: #EDECEC; border-radius: 5px' \n\
                class='last-sale-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <h4>Last sale</h4><p style='padding-left:15px'>Total: <span class='total_order'>0.00</span></p>\n\
                </div>");

                $("#dropHere .item-" + counts[0]).removeClass("last-sale-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $(this).remove();
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 96,
                    minWidth: 164
                });
            }
        }
    });
    
    var zIndex = 0;
    function make_draggable(elements){
        elements.draggable({
            containment: 'parent',
            start: function (e, ui) {
                ui.helper.css('z-index', ++zIndex);
            },
            stop: function (e, ui) {
            }
        });
    }

});


function setResolution(){
    var resWidth = [];
    resWidth[1] = "320px";
    resWidth[2] = "360px";
    resWidth[3] = "768px";
    resWidth[4] = "800px";
    resWidth[5] = "1024px";
    resWidth[6] = "1024px";
    resWidth[7] = "1280px";
    resWidth[8] = "1920px";

    var resHeight = [];
    resHeight[1] = "480px";
    resHeight[2] = "640px";
    resHeight[3] = "1024px";
    resHeight[4] = "1280px";
    resHeight[5] = "768px";
    resHeight[6] = "821px";
    resHeight[7] = "600px";
    resHeight[8] = "900px";

    var resSelected = document.getElementById('resolution').value;
    if(resSelected != 0 && resSelected != undefined){
        $('#init_layout .bar-widgets').attr('style', 'display: block');
        $('#init_layout #dropHere').attr('style', 'display: block');
        $('#text_init_layout').attr('style', 'display: none');
        document.getElementById('dropHere').style.width = resWidth[resSelected];
        document.getElementById('dropHere').style.height = resHeight[resSelected];
    } else {
        $('#text_init_layout').attr('style', 'display: block');
        $('#init_layout .bar-widgets').attr('style', 'display: none');
        $('#init_layout #dropHere').attr('style', 'display: none');
    }
}

function resetLayout(){
    $('#dropHere').html(' ');
}

$(document).on('submit', '#form_info_btn', function(e){
    e.preventDefault();
    var getForm = $('#form_info_btn').serializeArray();
    var form = [];
    $.each(getForm, function(i, field){
        form[i] = field.value;
    });
    var id = form[0];
    var name = form[1];
    var fontSize = form[2];
    var fontColor = form[3];
    var backgroundColor = form[4];
    var command = form[5];
    var functionBTN = form[6];
    var product = form[7];
    saveBtn(id, name, fontSize, fontColor, backgroundColor, command, functionBTN, product);
    $('#myModal').modal('hide');
});

function saveBtn(id, name, fontSize, fontColor, backgroundColor, command, functionBTN, product){
    var cssString = "";
    if(id != null && id != undefined && id != '') {
        var id = '#dropHere .item-'+id;
        if(name != null && name != undefined && name != '') {
            $(id).find('p').html(name);
        } 
        if(fontSize != null && fontSize != undefined && fontSize != '') {
            $(id).attr("data-font-size", fontSize);
            cssString += ' font-size:'+fontSize+'px;';
        } 
        if(fontColor != null && fontColor != undefined && fontColor != '') {
            $(id).attr("data-color", fontColor);
            cssString += ' color:'+fontColor+';';
        } 
        if(backgroundColor != null && backgroundColor != undefined && backgroundColor != '') {
            $(id).attr("data-background", backgroundColor);
            $(id).removeClass('btn-primary');
            cssString += ' background-color:'+backgroundColor+';';
        } 
        if(command != null && command != undefined && command != '') {
            $(id).attr("data-command", command);
        } 
        if(functionBTN != null && functionBTN != undefined && functionBTN != '') {
            $(id).attr("data-function", functionBTN);
        } 
        if(product != null && product != undefined && product != '') {
            $(id).attr("data-product", product);
        }
        $(id).attr("data-edit", 'true');
        $(id).attr('style', cssString);
    } else {
        alert('Error , try again');
    }
}

function editBtn(idElem, data){
    var idElem = ".item-"+idElem;
    $("#form_info_btn #nameBtn").val($(idElem).find('p').html());
    if(data['fontSize']){
        $("#form_info_btn #fontSize").val(data['fontSize']);
    }
    if(data['color']){
        $("#form_info_btn #cp8").val(data['color']);
    }
    if(data['background']){
        $("#form_info_btn #cp9").val(data['background']);
    }
    if(data['command']){
        $("#form_info_btn #command").val(data['command']);
    }
    if(data['function']){
        $("#form_info_btn #functionBTN").val(data['function']);
    }
    if(data['product']){
        $("#form_info_btn #product_select_btn").val(data['product']);
    }
}

function removeElem(idElem){
    var obj = $('#dropHere .item-'+ idElem);
    $(obj).remove();
    //console.log(obj[0].outerHTML);
}

function readFile() {
  if (this.files && this.files[0]) {
    var FR= new FileReader();
    FR.addEventListener("load", function(e) {
      document.getElementById(imgID).src = e.target.result;
      //document.getElementById("img-upload").src = e.target.result;
      //document.getElementById("b64").innerHTML = e.target.result;
    });  
    FR.readAsDataURL( this.files[0] );
  } 
}

function encodeImageFileAsURL() {
    var file = $('#dropHere .current-edit-image #inputFileToLoad');
    if(file){
        console.log($('#dropHere .current-edit-image p'));
        var filesSelected = file[0].files;
        if (filesSelected.length > 0) {
          var fileToLoad = filesSelected[0];
          var fileReader = new FileReader();
          fileReader.onload = function(fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data: base64
            var newImage = document.createElement('img');
            newImage.src = srcData;
            $('#dropHere .current-edit-image').attr('style', 'width: 150px; height:150px; background-image: url('+newImage.currentSrc+'); background-repeat: no-repeat; background-size: contain');
            $('#dropHere .current-edit-image p').remove();
            $('#dropHere .current-edit-image').removeClass('current-edit-image');
          }
          fileReader.readAsDataURL(fileToLoad);
        }
    }
  }
