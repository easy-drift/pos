﻿var authenticationRequest = {
    authenticationKey: '795180024C04479982560F61B3C2C06E'
};

var onAuthenticationSuccess = function (response) {
    console.log(response);
    // updateResult('Autenticado com sucesso fff' + '<br>' + 'Checkout GUID: ' + response.merchantCheckoutGuid);
    updateResult('<button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal"><span class="fa fa-credit-card" aria-hidden="true"> Cartão</span></button>');
};
var onAuthenticationError = function (error) {
    console.log(error);
    if (error.reasonCode == 7) {
        updateResult('<button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal"><span class="fa fa-credit-card" aria-hidden="true"> Cartão</span></button>');
    } else if (error.reasonCode == 4) {
        updateResult('<button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal"><span class="fa fa-credit-card" aria-hidden="true"> Cartão</span></button>');
    } else {
        updateResult('Código: ' + error.reasonCode + '<br>' + error.reason);
    }
};
var onPendingPayments = function (response) {
    console.log(response);
};

var checkout = CapptaCheckout.authenticate(authenticationRequest, onAuthenticationSuccess, onAuthenticationError, onPendingPayments);

var multiplePaymentsSessionInProgress = false;

function canStartMultiplePaymentsSession() {
    return multiplePaymentsSessionInProgress === false && $('input[name="rbMultiplePayments"]:checked').val() === 'true';
}

function startMultiplePayments() {
    try {
        var numberOfPayments = parseInt(document.getElementById('txtNumberOfPayments').value);

        checkout.startMultiplePayments(numberOfPayments, function () {
            alert('Sessão multiplos pagamentos encerrada!');
            document.getElementById('txtNumberOfPayments').value = 0;
            handlerMultiplePaymentsElements(false);

        });

        multiplePaymentsSessionInProgress = true;
        handlerMultiplePaymentsElements(true);
    } catch (ex) {
        alert(ex);
    }
}

function handlerMultiplePaymentsElements(disabled) {
    document.getElementById('txtNumberOfPayments').disabled = disabled;
    document.getElementById('rbUseMultiplePayments').disabled = disabled;
    document.getElementById('rbNotUseMultiplePayments').disabled = disabled;
}

var onPaymentSuccess = function (response) {

    var retorno = response.receipt.merchantReceipt.split("VALOR=");
    var controle = response.receipt.merchantReceipt.split("CONTROLE= ");
    localStorage.setItem('cartao', response.cardBrandName);
    var valorretorno = retorno[1].split(" ");
    var valorcontrole = controle[1].split(" ");
    localStorage.setItem('controle', valorcontrole[0]);
    var total = parseFloat(localStorage.getItem('remaining'));
    var valorfloat = valorretorno[1].split(",");
    var valorfloatemp = valorfloat[0].split(".");
    var valorfloatf = valorfloatemp[0]+valorfloatemp[1] + "." + valorfloat[1];
    var remaining = total - parseFloat(valorfloatf);
    localStorage.setItem('remaining', remaining);
    localStorage.setItem('vialoja', response.receipt.merchantReceipt);
    localStorage.setItem('viacliente', response.receipt.customerReceipt);
    $('#payment_remaining').html(remaining.formatMoney(2, ',', '.'));
    addPaymentMethodRowCartao(2,'Cartão', valorfloatf);
    
   // updatePayResult(response.receipt.merchantReceipt + '<br>' + response.receipt.customerReceipt);
};
var onPaymentError = function (error) {
    if (error.reasonCode == 7) {
        updateResult('<button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal"><span class="fa fa-credit-card" aria-hidden="true"> Cartão</span></button>');
    } else if (error.reasonCode == 4) {
        updateResult('<button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal"><span class="fa fa-credit-card" aria-hidden="true"> Cartão</span></button>');
    } else {
        updatePayResult('Código: ' + error.reasonCode + '<br>' + error.reason);
    }
};

function debitPayment() {
    if (canStartMultiplePaymentsSession()) {
        startMultiplePayments();
    }

    var amount = parseFloat(document.getElementById('txtDebitAmount').value.replace(',', ''));

    checkout.debitPayment({amount: amount}, onPaymentSuccess, onPaymentError);
}

function creditPayment() {
    if (canStartMultiplePaymentsSession()) {
        startMultiplePayments();
    }

    var elInstallmentType = document.getElementById("installmentType");
    var installmentType = elInstallmentType.options[elInstallmentType.selectedIndex].value;

    var installments = document.getElementById('txtCreditInstallments').value;

    var creditRequest = {
        amount: parseFloat(document.getElementById('txtCreditAmount').value.replace(',', '')),
        installments: installments === '' ? 0 : installments,
        installmentType: installmentType
    };

    checkout.creditPayment(creditRequest, onPaymentSuccess, onPaymentError);
}

function splittedDebitPayment() {
    if (canStartMultiplePaymentsSession()) {
        startMultiplePayments();
    }

    var splittedDebitRequest = {
        amount: parseFloat(document.getElementById('txtSplittedDebitAmount').value.replace(',', '')),
        installments: document.getElementById('txtSplittedDebitInstallments').value
    };

    checkout.splittedDebitPayment(splittedDebitRequest, onPaymentSuccess, onPaymentError);
}

function selectInstallmenteType(value) {
    if (value) {
        document.getElementById('installmentDetails').classList.add('show');
        return;
    }
    document.getElementById('installmentDetails').classList.remove('show');
}

function paymentReversal() {

    var paymentReversalRequest = {
        administrativePassword: document.getElementById('administrativePassword').value,
        administrativeCode: document.getElementById('administrativeCode').value
    };

    CapptaCheckout.paymentReversal(paymentReversalRequest, onPaymentSuccess, onPaymentError);
}

function pinpadInput() {
    var elInputType = document.getElementById("pinpadInputType");
    var inputType = elInputType.options[elInputType.selectedIndex].value;

    var success = function (response) {
        updateResult(response.pinpadValue);
    };

    var error = function (response) {
        updateResult(response.reason);
    };

    checkout.getPinpadInformation({inputType: inputType}, success, error);
}

function confirmPayments() {
    multiplePaymentsSessionInProgress = false;

    checkout.confirmPayments();

    alert('Pagamentos confirmados com sucesso!');
}

function undoPayments() {
    multiplePaymentsSessionInProgress = false;

    checkout.undoPayments();

    alert('Pagamentos desfeitos com sucesso!');
}

function updateResult(message) {
    document.getElementById('resposta').innerHTML = message;
}
function updatePayResult(message) {
    document.getElementById('resposta2').innerHTML = message;
}
function getValeuPayment() {

    var total = parseFloat(localStorage.getItem('total'));

    var paid = 0.0;
    $('input.payment-row-input').each(function (index) {
        value = ($(this).val() == '') ? 0.0 : parseFloat($(this).val());
        // console.log($(this).val(), value);
        paid += value;
    });
    var remaining = total - paid;
    localStorage.setItem('remaining', remaining);

    $('#txtDebitAmount').val(remaining.formatMoney(2, '.', ','));
    $('#txtCreditAmount').val(remaining.formatMoney(2, '.', ','));

    return remaining;
}


function addPaymentMethodRowCartao(id, name, valuecard) {
    var readOnly = false;
    var field_name = 'payment_method_value_8888';// + id;
    var input_id = 'payment-item-' + id + '-' + Math.floor((Math.random() * 5000) + 10);
    var container = $('#table-payment-list > tbody');
    var tr_id = 'payment-row-' + Math.floor((Math.random() * 5000) + 10);
    var tr = $('<tr></tr>').attr('id', tr_id);
    var payment_method_input = $('<input>').attr({
        'type': 'text',
        'name': field_name,
        'id': input_id,
        'value': parseFloat(valuecard),
        // 'disabled': 1
    }).addClass('table-input payment-row-input').css('width', '90px').prop('readOnly', readOnly);

    var td = [
        $('<td></td>').html(name),
        $('<td></td>').html(payment_method_input),
        $('<td></td>')
                .css('width', '50px')
                .html('')
    ];
    tr.append(td);
    container.append(tr);
    if (readOnly == false) {
        payment_method_input.focus();
        document.getElementById(input_id).select();
        localStorage.setItem('active_element', input_id);
    }

}

$(function () {

    $('#rbUseMultiplePayments').prop('checked', false);
    $('#rbNotUseMultiplePayments').prop('checked', true);

    // $('#txtDebitAmount').maskMoney();
    // $('#txtCreditAmount').maskMoney();
    // $('#txtSplittedDebitAmount').maskMoney();

    $('input[name=rbMultiplePayments]').change(function () {
        var isMultiplePayments = this.value === 'true' ? true : false;

        if (isMultiplePayments) {
            document.getElementById('txtNumberOfPayments').classList.remove('hide');
            document.getElementById('multiplePaymentsButtons').classList.remove('hide');
            return;
        }

        document.getElementById('txtNumberOfPayments').classList.add('hide');
        document.getElementById('multiplePaymentsButtons').classList.add('hide');
        multiplePaymentsSessionInProgress = false;
    });
});