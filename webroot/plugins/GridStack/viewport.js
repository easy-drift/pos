(function ($) {

    $.fn.easyViewport = function (options) {
        var opts = $.extend({}, $.fn.easyViewport.defaults, options);

        this.changeView = function (select, viewport) {
            select.on('change', function () {
                var resolution = $(this).find('option:selected').text();
                var x = resolution.indexOf("x");
                viewport.width(resolution.slice(0, x)).height(resolution.slice((x + 1)));
            });
        };

        this.create = function () {
            
            var options = '<option>800x600</option>' +
                    '<option>1024x768</option>',
            resolution = $("#resolution").append(options),
            height = opts.height.substr(0,opts.height.length-2),
            width = opts.width.substr(0,opts.width.length-2),            
            viewPort = $('#grid1').attr("style","width: "+opts.width+" !important; height: "+opts.height+" !important");
    
            $("#resolution option").each(function(){
                if ($(this).text() == width+"x"+height) {
                    $(this).attr('selected','selected');
                }
            });
    
            this.changeView(resolution, viewPort);
        };

        this.create();
        return this;
    };

    $.fn.easyViewport.defaults = {
        width: "1024px",
        height: "768px"
    };
}(jQuery));

var easyViewport = $("#grid1").easyViewport();