<?php
namespace Core;

use Application\Controllers;
use Application\Models;
use Core;
use Core\Helpers\Sql as Sql;

class Loader {
	private $inf;

	function __construct() {
		global $CONFIG;
		$this->inf = new Helpers\Inflector;

		// avoid use by unregistered POS
		$ignore = [
			'public/index',
			'session/login',
			'session/logout',
			'terminal/add'
		];
		$uri = str_replace(DIR.'/', '', $_SERVER['REQUEST_URI']);
		$uri_arr = explode('?', $uri);
		$uri = $uri_arr[0];

		if (($pos = $this->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]) == false) && 
			(in_array($uri, $ignore) == false)) {
			redirect_to('terminal/add');
		}

		return $this;

		//POS license number limit verification:  /////////////
		$settings = json_decode(file_get_contents(DOCROOT.'/app/config/config.json'), true);

		$url = $settings['sync']['server'].'/getcompanyData';

		// token located at config.json
		$fields = [
			'token' => $settings['sync']['token']
		];

		$company_data = json_decode(httpPOST($url, $fields, true), true);
		$pos_limit = $company_data['items']['terminals'];

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("COUNT(*) as qty");
		$db->from("Pos", "P");
		$db->where("P.last_online > DATE_SUB(NOW(), INTERVAL 10 MINUTE)");
		$db->where("P.pos_number != '{$CONFIG['device_id']}'");

		$pos_online = $db->run();
		$pos_online = $pos_online[0]['qty'];

		if (($pos_online > ($pos_limit - 1)) && (in_array($uri, $ignore) == false)) { //if limit exceed and user is in a mandatory logged in page, he can't access the system
			redirect_to('public/index?blocked=true');
		}
		else if (in_array($uri, $ignore) == false) { //if limit NOT exceed, but user is in a mandatory logged in page, he access the system, and the field last_online will be updated to inform that in the next 10min his POS is considered online
			$db = new Sql(get_pdo());
			$db->update("Pos");
			$db->set("last_online",date('Y-m-d H:i:s'));
			$db->where("pos_number = '{$CONFIG['device_id']}'");
			$db->run();
		}
	}

	public function view($view, $params=false) {

		global $urlParams;
		global $LANGUAGE;

		// extract params
		if (!$params) {
			$params = [];
		}

		// include meta into params
		$meta = [];
	    if ($meta_items = $this->model('Meta')->retrieve('all')) {
	        foreach ($meta_items as $key => $value) {
	            $meta[$value['data_type']] = $value['data_value'];
	        }
	    }
		$params['meta'] = $meta;
		extract($params);
		unset($params);

		$_language_file = DOCROOT.DS.'app'.DS.'languages'.DS.$LANGUAGE.'.php';
		if (file_exists($_language_file)) {
			include ($_language_file);
		}
		else {
			die('Language file not found');
		}

		$view_file = str_replace('/', DS, $view);
		unset($view);
		// views dir
		$viewDir = DOCROOT.DS.'app'.DS.'views'.DS;
		$viewAddr = $viewDir.$view_file.'.phtml';
		// check if file exists and give an exception
		if (file_exists($viewAddr)) {
			unset($viewDir);
			include($viewAddr);
		} else {
			die('<div class="system-error">View '.$viewAddr.' not found</div>');
		}

	}

	public function model($model) {
		global $urlParams;
		if (strstr($model, '_')) {
			$model = str_replace('_', ' ', $model);
			$model = ucwords($this->inf->singularize($model));
			$model = str_replace(' ', '_', $model);
		}
		else {
			$model = ucwords($this->inf->singularize($model));
		}
		$modelDir = DOCROOT.DS.'app'.DS.'models'.DS;
		$modelAddr = $modelDir.$model.'_Model.php';
		$class_name = 'Application\\Models\\'.$model.'_Model';
		// check if file exists and give an exception
		if (file_exists($modelAddr)) {
			if (!class_exists($class_name)) {
				include($modelAddr);
			}
			return new $class_name;
		}
		else {
			die('<div class="system-error">Model '.$modelAddr.' not found</div>');
		}
	}

	public function controller($controller) {

		global $urlParams;
		global $LANGUAGE;

		// load language file
		$_language_file = DOCROOT.DS.'app'.DS.'languages'.DS.$LANGUAGE.'.php';
		if (file_exists($_language_file)) {
			include ($_language_file);
		}
		else {
			die('Language file not found');
		}

		$controller = strtolower($this->pluralize($controller));
		$controllerDir = DOCROOT.DS.'app'.DS.'controllers'.DS;
		$controllerAddr = $controllerDir.$controller.'_controller.php';
		// check if file exists and give an exception
		if (file_exists($controllerAddr)) {
			if (class_exists(substr($controllerAddr, 0, strpos($controllerAddr, '.php')))) return false;
			include($controllerAddr);
		} else {
			die('<div class="system-error">Controller '.$controllerAddr.' not found</div>');
		}

	}

	/**
	* loadController() - loads a specific helper
	* @param string $helper
	* @return mixed
	*/
	public function helper($helper) {

		$file = DOCROOT.DS.'app'.DS.'helpers'.DS.$helper.'_helper.php';
		if (file_exists($file))
			include ($file);
	}
}