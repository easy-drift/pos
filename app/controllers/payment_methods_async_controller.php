<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Payment_Methods_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$payment_method_model = $this->load()->model('payment_method');
			$payment_method_model->set_values($__post);

			if ($id = $payment_method_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('customers/async/payment_method_add');
	}

	public function update($id) {
		global $__post;

		$payment_method = $this->load()->model('payment_method')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$payment_method->set_values($__post);

			if ($id = $payment_method->update()) {
				return $this->returnJson(200, $__post, $payment_method);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'payment_method' => $payment_method
		];

		$this->load()->view('customers/async/payment_method_update', $params);
	}
}