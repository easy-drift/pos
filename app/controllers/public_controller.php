<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Public_Controller extends Core\App_Controller {
	public function index() {
		global $CONFIG;
		global $__get;

		if (isset($__get['blocked'])) {
			$CONFIG['msg']['error'][] = $this->lang['blocked_by_limit'];
		}

		if (isset($__get['action']) && ($__get['action'] == 'pos_registered')) {
			$CONFIG['msg']['success'][] = $this->lang['pos_registered'];
		}

		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users
		];

		$this->load()->view('public/index', $params);
	}
}