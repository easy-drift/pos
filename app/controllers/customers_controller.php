<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Customers_Controller extends Core\App_Controller {
	private function getPos() {
		global $CONFIG;
		return $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
	}

	private function getPosSession() {
		global $CONFIG;
		$pos = $this->getPos();

		return $this->load()->model('Pos_session')->get_by(['pos_id'=>$pos->id, 'end_date'=>'null']);
	}

	public function index() {
		auth('yes');

		$tables_to_join = [
			'left' => ['Payment_methods'=>'PM.id']
		];
		  
		$person_model = $this->load()->model('People');
		$person_model->join()->prefix('P');
		$person_model->join()->tables($tables_to_join);
		$person_model->join()->fields(['PM.name'=>'payment_method_name']);

		$where = 'all';
		if (isset($_GET['search_by'])) {
			$where = ['P.'.$_GET['search_by'] => "LIKE %".$_GET['value']."%"];
		}

		$people = $person_model->retrieve($where, ['order'=>['name'=>'ASC']], 20);

		if ($people) {
			foreach ($people[0] as $key => $person) {
				$db = new Sql(get_pdo());
				$db->array_only = true;
				$db->select("A.address_city, A.address_zipcode, A.address_street, A.address_number, A.address_district");
				$db->from("Addresses", "A");
				$db->where("A.person_id = ".$person['id']);
				$db->where("A.active = 1");

				if ($address = $db->run()) {
					$address = $address[0];

					$people[0][$key]['address_city'] = $address['address_city'];
					$people[0][$key]['address_zipcode'] = $address['address_zipcode'];
					$people[0][$key]['address_street'] = $address['address_street'];
					$people[0][$key]['address_number'] = $address['address_number'];
					$people[0][$key]['address_district'] = $address['address_district'];
				}
			}
		}

		$params = [
			'people' => $people[0]
		];

		$this->load()->view('customers/index',$params);
	}

	public function add() {
		auth('yes');

		$person_model = $this->load()->model('People');

		$params = [
			'name' => ' '
		];

		$person_model->set_values($params);
		if ($id = $person_model->create()) {
			redirect_to('customers/update/'.$id);
		}
	}

	public function update() {
		auth('yes');
		global $__post;
		global $__get;
		global $urlParams;
		global $CONFIG;

		$person = $this->load()->model('People')->get_by_id($urlParams[0]);

		if ($__post) {

			$person->set_values($__post['person']);

			if ($person->update()) {
				if ($__post['address']) {
					$address_model = $this->load()->model('Addresses');
					foreach ($__post['address'] as $key => $value) {
						if ($value['name'] == '' || $value['address_street'] == '') {
							continue;
						}
						$value['person_id'] = $person->id;
						if ($addr = $address_model->get_by_id($key)) {
							$address_model->set_values($value);
							$address_model->update();
						}
						else {
							$address_model->set_values($value);
							$address_model->set('id', null);
							$address_model->create();
						}
					}
				}

				if ($__post['person_data']) {
					foreach ($__post['person_data'] as $key => $value) {
						if ($people_data_model = $this->load()->model('Person_data')->get_by(['data_type'=>$key,'person_id'=>$urlParams[0]])) {
							$people_data_model->set('data_value', $value);
							$people_data_model->update();
						}
						else {
							$people_data_model = $this->load()->model('Person_data');
							$people_data_model->set_values([
								'person_id' => $person->id,
								'data_type' => $key,
								'data_value' => $value
							]);
							$people_data_model->create();
						}
					}
				}

				if ($__post['settings']) {
					if ($settings = $this->load()->model('Person_settings')->get_by(['person_id'=>$person->id])) {
						$settings->set_values($__post['settings']);
						$settings->update();
					}
					else {
						$__post['settings']['person_id'] = $person->id;
						$settings_model = $this->load()->model('Person_settings');
						$settings_model->set_values($__post['settings']);
						$settings_model->create();
					}
				}
				$CONFIG['msg']['success'][] = $this->lang['success'];
			}
			else {
				$CONFIG['msg']['success'][] = $this->lang['registration_error'];
			}
		}

		if (isset($__get['action'])) {
			if ($__get['action'] == 'delete_address') {
				if ($addr = $this->load()->model('Addresses')->get_by_id($__get['id'])) {
					$addr->delete();
					redirect_to('customers/update/'.$person->id);
				}
			}
		}

		$users_types = $this->load()->model('users_types')->retrieve('all');
		$departments = $this->load()->model('departments')->retrieve('all');
		$jobs = $this->load()->model('jobs')->retrieve('all');
		$customers_gps = $this->load()->model('customer_groups')->retrieve('all');
		$countries = $this->load()->model('zz_countries')->retrieve('all');
		$payment_methods = $this->load()->model('payment_methods')->retrieve('all');
		$projects = $this->load()->model('projects')->retrieve('all');

		$settings = $this->load()->model('Person_settings')->get_by(['person_id'=>$person->id]);

		$user_model = $this->load()->model('users');
		$user_model->join()->prefix('U');
		$user_model->join()->tables([
			'inner' => [
				'People' => 'P.id'
			]
		]);
		$user_model->join()->fields(['P.name'=>'user_name']);
		$users = $user_model->retrieve('all');

		$addresses = $this->load()->model('Addresses')->retrieve(['person_id'=>$person->id]);

		$phone = $this->load()->model('Person_data')->get_by(['data_type'=>'phone','person_id'=>$urlParams[0]]);
		$email = $this->load()->model('Person_data')->get_by(['data_type'=>'email','person_id'=>$urlParams[0]]);
		$contact_phone = $this->load()->model('Person_data')->get_by(['data_type'=>'contact_phone','person_id'=>$urlParams[0]]);
		$contact_email = $this->load()->model('Person_data')->get_by(['data_type'=>'contact_email','person_id'=>$urlParams[0]]);

		$params = [
			'person'=>$person,
			'users'=>$users,
			'users_types'=>$users_types,
			'departments'=>$departments,
			'projects'=>$projects,
			'jobs'=>$jobs,
			'customers_gps'=>$customers_gps, 
			'countries'=>$countries,
			'settings'=>(array)$settings, 
			'payment_methods'=>$payment_methods,
			'addresses'=>$addresses,
			'phone'=>$phone,
			'email'=>$email,
			'contact_phone'=>$contact_phone,
			'contact_email'=>$contact_email,
		];

		$this->load()->view('customers/update',$params);
	}

	public function discounts() {

		$pace = 50;

		$_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 1;

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("CG.name as customer_group_name, PP.name as customer_name, PP.id as customer_id, PG.name as product_group_name, P.name as product_name, D.*");
		$db->from("Discounts", "D");
		$db->l_join("customer_groups CG","CG.id = D.customer_group_id");
		$db->l_join("people PP","PP.id = D.person_id");
		$db->l_join("product_groups PG","PG.id = D.product_group_id");
		$db->l_join("product P","P.id = D.product_id");
		$db->where("D.active = 1");
		$discounts = $db->run();

		$params = [
			'discounts' => $discounts
		];

		$this->load()->view('customers/discounts',$params);
	}

	public function discount_add() {
		global $__post;

		//$session_pos = $this->getPos();

		if ($__post) {
			$discount_model = $this->load()->model('Discount');
			$params = [
				'person_id' =>$__post['person_id'],
				'product_id' =>$__post['product_id'],
				'product_group_id' =>$__post['product_group_id'],
				'customer_group_id' =>$__post['customer_group_id'],
				'percent' =>$__post['percent'],
				'active' => 1
			];

			$discount_model->set_values($params);

			if ($id = $discount_model->create()) {
				redirect_to('customers/discounts');
			}
			else {

			}
		}

		$people = $this->load()->model('people')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$customer_groups = $this->load()->model('customer_groups')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$products = $this->load()->model('products')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$product_groups = $this->load()->model('product_groups')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'people'=>$people,
			'customer_groups'=>$customer_groups,
			'products'=>$products,
			'product_groups'=>$product_groups, 
		];

		$this->load()->view('customers/discount_add',$params);
	}

	public function discount_update() {
		global $__post;
		global $urlParams;

		//$session_pos = $this->getPos();

		$discount = $discount_model = $this->load()->model('Discounts')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'person_id' =>$__post['person_id'],
				'product_id' =>$__post['product_id'],
				'product_group_id' =>$__post['product_group_id'],
				'customer_group_id' =>$__post['customer_group_id'],
				'percent' =>$__post['percent'],
			];

			$discount->set_values($params);

			if ($id = $discount->update()) {
				$discount = $discount_model = $this->load()->model('Discounts')->get_by_id($urlParams[0]);
			}
			else {

			}
		}

		$people = $this->load()->model('people')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$customer_groups = $this->load()->model('customer_groups')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$products = $this->load()->model('products')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$product_groups = $this->load()->model('product_groups')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'people'=>$people,
			'customer_groups'=>$customer_groups,
			'products'=>$products,
			'product_groups'=>$product_groups, 
			'discount' => $discount,
		];

		$this->load()->view('customers/discount_update',$params);
	}

	public function payment_methods() {

		$pace = 50;

		$_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 1;

		$payment_method_model = $this->load()->model('Payment_method');
		$payment_methods = $payment_method_model->retrieve('all', '', $pace, $_GET['page']);

		$params = [
			'payment_methods' => $payment_methods,
			'pace' => $pace
		];

		$this->load()->view('customers/payment_methods',$params);
	}

	public function payment_method_add() {
		global $__post;

		//$session_pos = $this->getPos();

		if ($__post) {
			$payment_method_model = $this->load()->model('payment_method');
			$params = [
				'name' =>$__post['name'],
				'invoice' =>$__post['invoice'],
				'deadlinemonth' =>$__post['deadlinemonth'],
				'deadlineyear' =>$__post['deadlineyear'],
				'active' => 1
			];

			$payment_method_model->set_values($params);

			if ($id = $payment_method_model->create()) {
				redirect_to('customers/payment_methods');
			}
			else {

			}
		}

		$payment_methods = $this->load()->model('payment_methods')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'payment_methods'=>$payment_methods, 
		];

		$this->load()->view('customers/payment_method_add',$params);
	}

	public function customer_groups() {

		$pace = 50;

		$_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 1;

		  
		$customer_group_model = $this->load()->model('Customer_Group');
		$customer_groups = $customer_group_model->retrieve('all', '', $pace, $_GET['page']);

		$params = [
			'customer_groups' => $customer_groups,
			'pace' => $pace
		];

		$this->load()->view('customers/customer_groups',$params);
	}

	public function customer_group_add() {
		global $__post;

		//$session_pos = $this->getPos();

		if ($__post) {
			$customer_group_model = $this->load()->model('Customer_Group');
			$params = [
				'name' =>$__post['name'],
				'active' => 1
			];

			$customer_group_model->set_values($params);

			if ($id = $customer_group_model->create()) {
				redirect_to('customers/customer_groups');
			}
			else {

			}
		}


		$params = [
		];

		$this->load()->view('customers/customer_group_add',$params);
	}
}