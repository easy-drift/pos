<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Sizes_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$size_model = $this->load()->model('sizes');
			$size_model->set_values($__post);

			if ($id = $size_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('sizes/async/add');
	}

	public function update($id) {
		global $__post;

		$size = $this->load()->model('sizes')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$size->set_values($__post);

			if ($id = $size->update()) {
				return $this->returnJson(200, $__post, $size);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'size' => $size
		];

		$this->load()->view('sizes/async/update', $params);
	}
}