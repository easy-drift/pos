<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Departments_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$department_model = $this->load()->model('Departments');
			$department_model->set_values($__post);

			if ($id = $department_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('departments/async/add');
	}

	public function update($id) {
		global $__post;

		$department = $this->load()->model('Departments')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$department->set_values($__post);

			if ($id = $department->update()) {
				return $this->returnJson(200, $__post, $department);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'department' => $department
		];

		$this->load()->view('departments/async/update', $params);
	}
}