<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Departments_Controller extends Core\App_Controller {
	public function index() {
		auth('yes');

		$departments = $this->load()->model('Departments')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'departments' => $departments
		];

		$this->load()->view('departments/index', $params);
	}

	public function update() {
		global $__post;
		global $urlParams;

		$department = $department_model = $this->load()->model('Departments')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
			];

			$department->set_values($params);

			if ($id = $department->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'department' => $department,
		];

		$this->load()->view('departments/update',$params);
	}
}