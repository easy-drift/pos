<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Setup_Controller extends Core\App_Controller {
	public function index() {
		auth('yes');
		global $__post;
		global $CONFIG;

		$languages = $this->load()->model('Language')->retrieve('all');

		$meta_model = $this->load()->model('Meta');

		$meta = getMeta();

		if (isset($_POST['meta'])) {
			foreach ($_POST['meta'] as $key => $value) {
				$data = [
					'data_type' => $key,
					'data_value' => $value
				];
				if (isset($meta[$key])) {
					$meta_obj = $meta_model->get_by(['data_type' => $key]);
					$meta_obj->set_values($data);
					$meta_obj->update();
				}
				else {
					$meta_model->set_values($data);
					$meta_model->create();
				}
			}
			$CONFIG['msg']['success'][] = $this->lang['success'];
		}

		// get meta again
		$meta = getMeta();

		// get local settings
		$settings = json_decode(file_get_contents(DOCROOT.'/app/config/config.json'), true);
		// token located at config.json
		$url = $settings['sync']['server'].'/getcompanyData';
		$fields = [
			'token' => $settings['sync']['token']
		];

		$company_data = json_decode(httpPOST($url, $fields, true), true);

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("COUNT(*) as qty");
		$db->from("Pos", "P");
		$db->where("P.last_online > DATE_SUB(NOW(), INTERVAL 10 MINUTE)");

		$current_connections = $db->run();
		$current_connections = $current_connections[0]['qty'];

		$params = [
			'languages' => $languages,
			'company_data' => $company_data['items'],
			'current_connections' => $current_connections,
			'device_id' => $CONFIG['device_id'],
			'meta' => $meta
		];

		$this->load()->view('setup/index', $params);
	}
}