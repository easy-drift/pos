<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Groups_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$product_group_model = $this->load()->model('Product_groups');
			$product_group_model->set_values($__post);

			if ($id = $product_group_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$departments = $this->load()->model('Departments')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'departments' => $departments
		];

		$this->load()->view('groups/async/add', $params);
	}

	public function update($id) {
		global $__post;

		$group = $this->load()->model('Product_groups')->get_by_id($id);
		$departments = $this->load()->model('Departments')->retrieve('all', ['order'=>['name'=>'ASC']]);

		if ($__post) {
			header("Content-type: application/json");

			
			$group->set_values($__post);

			if ($id = $group->update()) {
				return $this->returnJson(200, $__post, $group);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'group' => $group,
			'departments' => $departments
		];

		$this->load()->view('groups/async/update', $params);
	}
}