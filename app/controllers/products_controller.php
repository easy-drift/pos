<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Products_Controller extends Core\App_Controller {
	public function index() {
		auth('yes');
		global $__get;

		$pace = 20;

		if ($__get) {
			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("*");
			$db->from("Products");
			$db->where($__get['search_by']." LIKE '%".$__get['value']."%'");
			$db->limit($pace);
			$products = [$db->run()];
			// _dump($products);
		}
		else {

			$_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 1;

			$products = $this->load()->model('Products')->retrieve('all', ['order'=>['name'=>'ASC']], $pace, $_GET['page']);
		}

		$params = [
			'products' => $products,
			'pace' => $pace
		];

		$this->load()->view('products/index', $params);
	}

	public function add() {
		auth('yes');

		$product_model = $this->load()->model('Products');

		$params = [
			'name' => ''
		];

		$product_model->set_values($params);
		if ($id = $product_model->create()) {
			redirect_to('products/update/'.$id);
		}
	}

	public function update($id) {
		global $__post;
		global $CONFIG;
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($id)) {
			// return $this->index();
			return false;
		}

		if ($__post) {
			// _dump($__post, 1);

			if (!isset($__post['product']['active'])) {
				$__post['product']['active'] = '0';
			}
			if (!isset($__post['product']['has_components'])) {
				$__post['product']['has_components'] = '0';
			}
			if (!isset($__post['product']['components_show_order_subitens'])) {
				$__post['product']['components_show_order_subitens'] = '0';
			}

			$data = $__post['product'];

			$product->set_values($data);

			if ($product->update()) {
				$CONFIG['msg']['success'][] = 'Cadastro efetuado';
			}
		}

		$tables_to_join = [
			'inner' => [
				'companies' => 'C.id'
			]
		];
		$supplier_model = $this->load()->model('Supplier');
		$supplier_model->join()->tables($tables_to_join);
		$supplier_model->join()->prefix('S');
		$supplier_model->join()->fields(['C.name'=>'company_name']);
		$suppliers = $supplier_model->retrieve(['S.active'=>'1']);

		$taxes = $this->load()->model('Taxes')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$collections = $this->load()->model('Product_Collections')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$manufacturers = $this->load()->model('Manufacturers')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$serial_numbers = $this->load()->model('Product_Serial_Numbers')->retrieve(['product_id'=>$product->id]);
		$units = $this->load()->model('Units')->retrieve('all');
		$departments = $this->load()->model('Departments')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$projects = $this->load()->model('Projects')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'product' => $product,
			'taxes' => $taxes,
			'collections' => $collections,
			'manufacturers' => $manufacturers,
			'suppliers' => $suppliers,
			'serial_numbers' => $serial_numbers,
			'units' => $units,
			'departments' => $departments,
			'projects' => $projects
		];

		$this->load()->view('products/update', $params);
	}

	public function groups() {
		auth('yes');

		$group_model = $this->load()->model('Product_groups');
		$tables = [
			'left' => [
				'departments' => 'D.id'
			]
		];
		$group_model->join()->prefix('G');
		$group_model->join()->tables($tables);
		$group_model->join()->fields(['D.name' => 'department_name']);
		$groups = $group_model->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'groups' => $groups
		];

		$this->load()->view('products/groups', $params);
	}
}