<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Suppliers_Controller extends Core\App_Controller {
	public function index() {
		auth('yes');

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("C.*, S.id, PM.name as payment_method_name");
		$db->from("Suppliers", "S");
		$db->join("Companies C", "C.id = S.company_id");
		$db->join("Payment_methods PM", "PM.id = C.payment_mtd_id");
		$suppliers = $db->run();

		$params = [
			'suppliers' => $suppliers
		];

		$this->load()->view('suppliers/index', $params);
	}
}