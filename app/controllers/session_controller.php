<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Session_Controller extends Core\App_Controller {
	public function login() {
		global $__post;
		global $__server;

		if (strtolower($__server['REQUEST_METHOD']) == 'post') {
			if (!$user = $this->load()->model('Users')->get_by_id($__post['user_id'])) {
				$this->returnJson(404);
			}
			else {
				if (password_verify($__post['secret'], $user->password)) {
					// login
					$_SESSION['app'] = [
						'user' => [
							'id' => $user->id,
							'login' => $user->login
						]
					];
					$this->returnJson(200, $_POST);
				}
				else {
					// invalid password
					$this->returnJson(401);
				}
			}
		}
		else {
			$this->returnJson(405, $__server['REQUEST_METHOD']);
		}
	}

	public function logout() {
        session_destroy();
        session_unset();
        redirect_to('public/index');
    }
}