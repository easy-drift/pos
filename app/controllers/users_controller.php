<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Users_Controller extends Core\App_Controller {

	private function getPos() {
		global $CONFIG;
		return $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
	}

	private function getPosSession() {
		global $CONFIG;
		$pos = $this->getPos();

		return $this->load()->model('Pos_session')->get_by(['pos_id'=>$pos->id, 'end_date'=>'null']);
	}

	public function index() {
		$db = new Sql(get_pdo());
		$db->select("U.*, UT.name as type, J.name as job, P.name as full_name, D.name as department, Pos.nickname as pos");
		$db->from("Users", "U");
		$db->l_join("Users_type UT", "UT.id = U.users_type_id");
		$db->l_join("Job J", "J.id = U.job_id");
		$db->l_join("People P", "P.id = U.person_id");
		$db->l_join("Department D", "D.id = P.department_id");
		$db->l_join("Pos Pos", "Pos.id = P.pos_id");
		$users = $db->run();

		$params = [
			'users' => $users
		];

		$this->load()->view('users/index',$params);
	}

	public function add() {
		global $__post;

		$session_pos = $this->getPos();

		if ($__post) {
			$person_model = $this->load()->model('People');

			$params = [
				'pos_id' => $session_pos->id,
				'customers_gp_id' => $__post['customers_gp_id'],
				'zz_country_id' => $__post['zz_country_id'],
				'people_settings_id' => $__post['people_settings_id'],
				'payments_mtd_id' => $__post['payments_mtd_id'],
				'department_id' => $__post['department_id'],
				'name' => $__post['name'],
				'document_type' => $__post['document_type'],
				'document_number' => $__post['document_number'],
				'birth_date' => $__post['birth_date'],
				'personal_number' => $__post['personal_number'],
				'contact_person' => $__post['contact_person'],
				'info' => $__post['info'],
				'genre' => $__post['genre'],
				'electronic_identification' => $__post['electronic_identification'],
				'active' => 1
			];

			$person_model->set_values($params);

			if ($id = $person_model->create()) {

				$user_model = $this->load()->model('Users');
				$data = [
					'login' => $__post['login'],
					'password' => password_hash($__post['password'], PASSWORD_DEFAULT),
					'users_type_id' => $__post['users_type_id'],
					'job_id' => $__post['job_id'],
					'person_id' => $id,
					'active' => 1
				];

				$user_model->set_values($data);

				if ($id = $user_model->create(1)) {
					redirect_to('users/index');
				}
				else {

				}
			}
			else {

			}
		}

		$users_types = $this->load()->model('users_types')->retrieve('all');
		$departments = $this->load()->model('departments')->retrieve('all');
		$jobs = $this->load()->model('jobs')->retrieve('all');
		$customer_groups = $this->load()->model('customer_groups')->retrieve('all');
		$countries = $this->load()->model('zz_countries')->retrieve('all');
		$person_settings = $this->load()->model('person_settings')->retrieve('all');
		$payment_methods = $this->load()->model('payment_methods')->retrieve('all');

		$params = [
			'users_types'=>$users_types,
			'departments'=>$departments,
			'jobs'=>$jobs,
			'customer_groups'=>$customer_groups, 
			'countries'=>$countries,
			'person_settings'=>$person_settings, 
			'payment_methods'=>$payment_methods,
		];

		$this->load()->view('users/add',$params);
	}

	public function update() {
		global $__post;
		global $urlParams;

		$session_pos = $this->getPos();

		$user = $user_model = $this->load()->model('Users')->get_by_id($urlParams[0]);
		$person = $person_model = $this->load()->model('People')->get_by_id($user->person_id);

		if ($__post) {
			$params = [
				'pos_id' => $session_pos->id,
				'customers_gp_id' => isset ($__post['customers_gp_id']) ? $__post['customers_gp_id'] : NULL,
				'zz_country_id' => $__post['zz_country_id'],
				'people_settings_id' => isset ($__post['people_settings_id']) ? $__post['people_settings_id'] : NULL,
				'payments_mtd_id' => isset ($__post['payments_mtd_id']) ? $__post['payments_mtd_id'] : NULL,
				'department_id' => $__post['department_id'],
				'name' => $__post['name'],
				'document_type' => $__post['document_type'],
				'document_number' => $__post['document_number'],
				'birth_date' => $__post['birth_date'],
				'personal_number' => $__post['personal_number'],
				'contact_person' => $__post['contact_person'],
				'info' => $__post['info'],
				'genre' => $__post['genre'],
				'electronic_identification' => $__post['electronic_identification'],
				'active' => 1
			];

			$person_model->set_values($params);

			if ($person_model->update()) {
				$person = $person_model = $this->load()->model('People')->get_by_id($user->person_id);

				$data = [
					'login' => $__post['login'],
					'users_type_id' => $__post['users_type_id'],
					'job_id' => $__post['job_id'],
					'active' => 1
				];

				if ($__post['password']) {
					$data['password'] = password_hash($__post['password'], PASSWORD_DEFAULT);
				}

				$user_model->set_values($data);

				if ($user_model->update()) {
					$user = $user_model = $this->load()->model('Users')->get_by_id($urlParams[0]);
				}
				else {

				}
			}
			else {

			}
		}

		if ($person->zz_country_id) {
			$country = $this->load()->model('Zz_Country')->get_by_id($person->zz_country_id);
		}

		$users_types = $this->load()->model('users_types')->retrieve('all');
		$departments = $this->load()->model('departments')->retrieve('all');
		$jobs = $this->load()->model('jobs')->retrieve('all');
		$customer_groups = $this->load()->model('customer_groups')->retrieve('all');
		$countries = $this->load()->model('zz_countries')->retrieve('all');
		$person_settings = $this->load()->model('person_settings')->retrieve('all');
		$payment_methods = $this->load()->model('payment_methods')->retrieve('all');

		$params = [
			'person'=>$person,
			'user'=>$user,
			'users_types'=>$users_types,
			'departments'=>$departments,
			'jobs'=>$jobs,
			'customer_groups'=>$customer_groups, 
			'countries'=>$countries,
			'person_settings'=>$person_settings, 
			'payment_methods'=>$payment_methods,
			'country_id'=>$country->id,
		];

		$this->load()->view('users/update',$params);
	}
}