<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Colors_Controller extends Core\App_Controller {
	public function index() {
		$colors = $this->load()->model('Colors')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'colors' => $colors
		];

		$this->load()->view('colors/index',$params);
	}

	public function add() {
		global $__post;

		if ($__post) {
			$color_model = $this->load()->model('colors');

			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
				'active' => 1
			];

			$color_model->set_values($params);

			if ($id = $color_model->create()) {
				redirect_to('colors/index');
			}
			else {

			}
		}

		$this->load()->view('colors/add');
	}

	public function update() {
		global $__post;
		global $urlParams;

		$color = $color_model = $this->load()->model('colors')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
			];

			$color->set_values($params);

			if ($id = $color->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'color' => $color,
		];

		$this->load()->view('colors/update',$params);
	}
}