<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Sizes_Controller extends Core\App_Controller {
	public function index() {
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("S.*");
		$db->from("Sizes", "S");
		$db->where("S.active IN (1,2)");
		$db->order("S.name","asc");
		$sizes = $db->run();

		$params = [
			'sizes' => $sizes
		];

		$this->load()->view('sizes/index',$params);
	}

	public function add() {
		global $__post;

		if ($__post) {
			$size_model = $this->load()->model('Sizes');

			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
				'active' => 1
			];

			$size_model->set_values($params);

			if ($id = $size_model->create()) {
				redirect_to('sizes/index');
			}
			else {

			}
		}

		$this->load()->view('sizes/add');
	}

	public function update() {
		global $__post;
		global $urlParams;

		$size = $size_model = $this->load()->model('Sizes')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
			];

			$size->set_values($params);

			if ($id = $size->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'size' => $size,
		];

		$this->load()->view('sizes/update',$params);
	}
}