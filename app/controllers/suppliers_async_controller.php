<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Suppliers_Async_Controller extends Core\App_Controller {
	private function getPos() {
		global $CONFIG;
		return $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
	}

	private function getPosSession() {
		global $CONFIG;
		$pos = $this->getPos();

		return $this->load()->model('Pos_session')->get_by(['pos_id'=>$pos->id, 'end_date'=>'null']);
	}

	public function add() {
		global $__post;

		$session_pos = $this->getPos();

		if ($__post) {
			header("Content-type: application/json");

			$__post['pos_id'] = $session_pos;
			$company_model = $this->load()->model('Companies');
			$company_model->set_values($__post);

			if ($id = $company_model->create()) {
				$supplier_model = $this->load()->model('Suppliers');
				$supplier_model->set_values(['company_id'=>$id]);

				if ($id = $supplier_model->create()) {
					return $this->returnJson(200, $__post, $id);
				}
				else {
					return $this->returnJson(500, $__post);
				}
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$payment_method_model = $this->load()->model('Payment_Method');
		$payment_methods = $payment_method_model->retrieve('all');

		$params = [
			'payment_methods' => $payment_methods
		];

		$this->load()->view('suppliers/async/add',$params);
	}

	public function update($id) {
		global $__post;

		$supplier = $this->load()->model('Suppliers')->get_by_id($id);
		$company = $this->load()->model('Companies')->get_by_id($supplier->company_id);

		if ($__post) {
			header("Content-type: application/json");
			
			$company->set_values($__post);

			if ($id = $company->update()) {
				return $this->returnJson(200, $__post, $supplier);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$db = new Sql(get_pdo());
		$db->array_only = false;
		$db->select("C.*, S.id, PM.name as payment_method_name");
		$db->from("Suppliers", "S");
		$db->join("Companies C", "C.id = S.company_id");
		$db->join("Payment_methods PM", "PM.id = C.payment_mtd_id");
		$db->where("S.id = ".$supplier->id);
		$supplier = $db->run();

		$payment_method_model = $this->load()->model('Payment_Method');
		$payment_methods = $payment_method_model->retrieve('all');

		$params = [
			'supplier' => $supplier,
			'payment_methods' => $payment_methods
		];

		$this->load()->view('suppliers/async/update', $params);
	}
}