<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Admin_Controller extends Core\App_Controller {
	public function index() {
		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users
		];

		$this->load()->view('admin/index');
	}
}