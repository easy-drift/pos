<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Countries_Controller extends Core\App_Controller {
	public function index() {
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("C.*");
		$db->from("Zz_Countries", "C");
		$db->where("C.active IN (1,2)");
		$countries = $db->run();

		$params = [
			'countries' => $countries
		];

		$this->load()->view('countries/index',$params);
	}

	public function add() {
		global $__post;

		if ($__post) {
			$country_model = $this->load()->model('Zz_Countries');

			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
				'active' => 1
			];

			$country_model->set_values($params);

			if ($id = $country_model->create()) {
				redirect_to('countries/index');
			}
			else {

			}
		}

		$this->load()->view('countries/add');
	}

	public function update() {
		global $__post;
		global $urlParams;

		$country = $country_model = $this->load()->model('Zz_Countries')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
				'code' => $__post['code'],
			];

			$country->set_values($params);

			if ($id = $country->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'country' => $country,
		];

		$this->load()->view('countries/update',$params);
	}
}