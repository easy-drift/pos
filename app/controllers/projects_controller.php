<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Projects_Controller extends Core\App_Controller {
	public function index() {
		auth('yes');

		$projects = $this->load()->model('Projects')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'projects' => $projects
		];

		$this->load()->view('projects/index', $params);
	}

	public function update() {
		global $__post;
		global $urlParams;

		$project = $project_model = $this->load()->model('Projects')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
			];

			$project->set_values($params);

			if ($id = $project->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'project' => $project,
		];

		$this->load()->view('projects/update',$params);
	}
}