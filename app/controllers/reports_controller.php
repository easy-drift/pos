<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Reports_Controller extends Core\App_Controller {
	public function terminal() {
		auth('yes');
		global $CONFIG;

		if (!isset($_GET['period'])) {
			$_GET['period'] = date('d/m/Y') . ' - ' . date('d/m/Y');
		}
		if (!isset($_GET['order_type'])) {
			$_GET['order_type'] = -1;
		}
		if (!isset($_GET['status'])) {
			$_GET['status'] = -1;
		}

		$order = false;

		if ($_GET) {
			$period_arr = explode(' - ', $_GET['period']);
			$start = format_date($period_arr[0]);
			$end = format_date($period_arr[1]);

			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("O.*, P.name AS client_name, POS.nickname, PM.name AS payment_method_name");
			$db->from("Orders", "O");
			$db->join("Pos_sessions PS", "PS.id = O.pos_session_id");
			$db->join("Pos POS", "POS.id = PS.pos_id");
			$db->join("People P", "P.id = O.person_id");
			$db->l_join("Order_payments OP", "OP.order_id = O.id");
			$db->l_join("Payment_methods PM", "PM.id = OP.payment_method_id");
			$db->where("O.date_create BETWEEN '".$start." 00:00:00' AND '".$end." 23:59:59'");
			$db->where("O.amount > 0");
			if (isset($_GET['order_type']) && $_GET['order_type'] >= 0) {
				$db->where("O.order_type = '".$_GET['order_type']."'");
			}
			if (isset($_GET['status']) && $_GET['status'] >= 0) {
				$db->where("O.status = '".$_GET['status']."'");
			}
			if (isset($_GET['user_id']) && $_GET['user_id'] != '') {
				$db->where("O.user_id = '".$_GET['user_id']."'");
			}
			if (isset($_GET['client_name']) && $_GET['client_name'] != '') {
				$db->where("P.name LIKE '%".$_GET['client_name']."%'");
			}
			$db->group("O.id");
			$db->order("O.id", "DESC");
			$orders = $db->run();
		}

		$pos_list = $this->load()->model('Pos')->retrieve(['active'=>'1']);

		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users,
			'pos_list' => $pos_list,
			'orders' => $orders,
			'order_types' => $CONFIG['order_types'],
			'order_status' => $CONFIG['order_status']
		];

		$_SESSION['back_url'] = WWWROOT . '/reports/terminal?' . $_SERVER['QUERY_STRING'];

		$this->load()->view('reports/terminal', $params);
	}

	public function daily_takings() {
		auth('yes');
		global $CONFIG;

		if (!isset($_GET['period'])) {
			$_GET['period'] = date('d/m/Y') . ' - ' . date('d/m/Y');
		}

		$period_arr = explode(' - ', $_GET['period']);
		$start = format_date($period_arr[0]);
		$end = format_date($period_arr[1]);

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("PSC.*, U.login");
		$db->from("Pos_session_closures", "PSC");
		$db->join("Pos_sessions PS", "PS.id = PSC.pos_session_id");
		$db->join("Users U", "U.id = PSC.user_id");
		$db->where("PSC.date_create BETWEEN '".$start." 00:00:00' AND '".$end." 23:59:59'");
		$report = $db->run();

		$params = [
			'report' => $report
		];

		$this->load()->view('reports/daily_takings', $params);
	}
}