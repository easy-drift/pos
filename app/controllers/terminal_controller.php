<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;
use Core\Helpers\File as File;

class Terminal_Controller extends Core\App_Controller {

	private function getPos() {
		global $CONFIG;
		return $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
	}

	private function getPosSession() {
		global $CONFIG;
		$pos = $this->getPos();

		return $this->load()->model('Pos_session')->get_by(['pos_id'=>$pos->id, 'end_date'=>'null']);
	}

	public function index() {
		auth('yes');
		global $CONFIG;

		$users = $this->load()->model('Users')->retrieve('all');
		$pos = $this->getPos();

		// check if any pos session is open for this pos
		if (!$pos_session = $this->getPosSession()) {
			redirect_to('terminal/open');
		}

		if (isset($_GET['error'])) {
			if ($_GET['error'] == 'parked_orders') {
				$CONFIG['msg']['error'][] = $this->lang['error_parked_orders'];
			}
		}

		// cancel order
		if (isset($_GET['cancel']) && isset($_SESSION['app']['order']['id'])) {
			$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);

			$order->set('in_use', '0');
			$order->set('type', '0');
			$order->set('status', '3');
			$order->set('active', '0');
			$order->set('date_delete', now());
			if ($order->update()) {
				unset($_SESSION['app']['order']);
				redirect_to('terminal/index');
			}
		}
		// clear terminal screen
		else if (isset($_GET['clear']) && isset($_SESSION['app']['order']['id'])) {
			unset($_SESSION['app']['order']);
			redirect_to('terminal/index');
		}
		// load order into terminal screen
		else if (isset($_GET['order_id']) && ($order = $this->load()->model('Orders')->get_by_id($_GET['order_id']))) {
			$_SESSION['app']['order']['id'] = $order->id;
		}

		$tabs = $this->load()->model('Favorite_Button_Tabs')->retrieve('all');

		$tables_to_join = ['inner' => ['favorite_button_tabs' => 'FBT.id']];
		$button_model = $this->load()->model('Favorite_Buttons');
		$button_model->join()->prefix('FB');
		$button_model->join()->tables($tables_to_join);
		$button_model->join()->fields(['FBT.name' => 'tab_name']);

		$buttons = [];
		if ($fav_buttons = $button_model->retrieve('all')) {
			foreach ($fav_buttons as $key => $value) {
				$tab_id = $value['favorite_button_tab_id'];
				if (!isset($buttons[$tab_id])) {
					$buttons[$tab_id] = [];
				}
				$buttons[$tab_id][$value['id_html']] = $value;
			}
		}

		$meta = getMeta();

		// check company logo
		$picture = WEBROOT . '/images/logo-placeholder.jpg';
		if (isset($meta['company_logo']) && $meta['company_logo'] != '') {
			$picture = WEBROOT . '/images/' . $meta['company_logo'];
		}

		$params = [
			'users' => $users,
			'tabs' => $tabs,
			'buttons' => $buttons,
			'pos_session' => $pos_session,
			'meta' => $meta,
			'picture' => $picture
		];

		$this->load()->view('terminal/index', $params);
	}

	public function favorite_buttons() {
		auth('yes');
		global $__post;
		global $CONFIG;

		$_GET['tab_id'] = isset($_GET['tab_id']) ? $_GET['tab_id'] : 1;

		// remove tab
		if ((@$_GET['action'] == 'delete') && isset($_GET['tab_id']) && ($_GET['tab_id'] != '1')) {
			$tab = $this->load()->model('Favorite_Button_Tabs')->get_by_id($_GET['tab_id']);
			$tab->delete();
			redirect_to('terminal/config');
		}

		if ($__post) {
			// update tab
			if (isset($__post['tab_id'])) {
				$tab = $this->load()->model('Favorite_Button_Tabs')->get_by_id($__post['tab_id']);
				$tab->set('name', $__post['tab_name']);
				if ($tab->update()) {
					$CONFIG['msg']['success'][] = 'All done';
				}
			}
			// update button
			else if ($__post['id'] != '') {
				$button = $this->load()->model('Favorite_Buttons')->get_by_id($__post['id']);
				$button->set_values($__post);
				if ($button->update()) {
					$CONFIG['msg']['success'][] = 'All done';
				}
			}
			// register button
			else {
				$button_model = $this->load()->model('Favorite_Buttons');
				$button_model->set_values($__post);
				if ($button_model->create()) {
					$CONFIG['msg']['success'][] = 'All done';
				}
			}
		}

		$tabs = $this->load()->model('Favorite_Button_Tabs')->retrieve('all');

		$buttons = [];
		if ($fav_buttons = $this->load()->model('Favorite_Buttons')->retrieve(['favorite_button_tab_id'=>$_GET['tab_id']])) {
			foreach ($fav_buttons as $key => $value) {
				$buttons[$value['id_html']] = $value;
			}
		}

		$selectTab = function($id) {
			if (isset($_GET['tab_id']) && $_GET['tab_id'] == $id) {
				return 'active';
			}
			return '';
		};

		$tab = $this->load()->model('Favorite_Button_Tabs')->get_by_id($_GET['tab_id']);

		$params = [
			'tabs' => $tabs,
			'buttons' => $buttons,
			'selectTab' => $selectTab,
			'current_tab' => $tab
		];

		$this->load()->view('terminal/favorite_buttons', $params);
	}

	public function config() {
		auth('yes');
		global $__post;
		global $CONFIG;

		if (isset($_FILES['picture']) && $_FILES['picture']['error']== 0) {
			$file = new File($_FILES['picture']);
			$file->set_allowed_types(array('png', 'jpg', 'gif'));
			// $file->set_force_create_dir(true);
			$file->set_destination('webroot/images');
			$file->set_max_size(1.0);
			$file->set_filename('company_logo');
			$file->resize('600','auto');
			if ($filename = $file->upload()) {
				$__post['meta']['company_logo'] = str_replace(DOCROOT.'/webroot/images/', '', $filename);
				$CONFIG['msg']['success'][] = 'Ferdig';
			}
			else {
				if ($errors = $file->get_errors()) {
					foreach ($errors as $key => $value) {
						$CONFIG['msg']['error'][] = $value;
					}
				}
				else {
					$CONFIG['msg']['error'][] = $this->lang['message_329']; 
				}
			}
		}

		if ($__post) {
			$meta_model = $this->load()->model('Meta');

			// set detaulf values
			if (empty($__post['virtual_keyboard'])) {
				$__post['virtual_keyboard'] = '0';
			}

			// register meta
			foreach ($__post['meta'] as $key => $value) {
				$meta_model->registerMeta($key, $value);
			}
			$CONFIG['msg']['success'][] = 'Cadastro efetuado';
		}

		$meta = getMeta();

		// check company logo
		$picture = WEBROOT . '/images/logo-placeholder.jpg';
		if (isset($meta['company_logo']) && $meta['company_logo'] != '') {
			$picture = WEBROOT . '/images/' . $meta['company_logo'];
		}

		$params = [
			'meta' => $meta,
			'picture' => $picture
		];

		$this->load()->view('terminal/config', $params);
	}

	public function add() {
		auth('yes');
		global $CONFIG;
		global $__post;

		if ($pos = $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']])) {
			redirect_to('public/index');
		}

		if ($__post) {
			$pos_model = $this->load()->model('Pos');
			$__post['pos_number'] = $CONFIG['device_id'];
			$pos_model->set_values($__post);

			if ($pos_model->create()) {
				redirect_to('public/index?action=pos_registered');
			}
		}

		$params = [
			'device_id' => $CONFIG['device_id']
		];

		$this->load()->view('terminal/add', $params);
	}

	public function open() {
		auth('yes');
		global $CONFIG;

		$pos = $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
		$user = $this->load()->model('Users')->get_by_id($_SESSION['app']['user']['id']);

		if ($this->post) {
			$session_model = $this->load()->model('Pos_session');
			$this->post['pos_id'] = $pos->id;
			$this->post['user_id'] = $user->id;
			$this->post['date_start'] = date('Y-m-d H:i:s');

			$session_model->set_values($this->post);
			if ($session_model->create()) {
				redirect_to('terminal/index');
			}
		}

		$params = [
			'pos' => $pos,
			'user' => $user
		];

		$this->load()->view('terminal/open', $params);
	}

	public function close() {
		auth('yes');
		global $CONFIG;

		$pos = $this->getPos();
		$user = $this->load()->model('Users')->get_by_id($_SESSION['app']['user']['id']);

		if (!$pos_session = $this->getPosSession()) {
			redirect_to('terminal/open');
		}

		// check if there are any unpaid parked orders
		$db = new Sql(get_pdo());
		$db->select("*");
		$db->from("Orders");
		$db->where("amount > '0'");
		$db->where("amount_outstanding > '0'");
		$db->where("order_type = '0'");
		$db->where("status IN (0, 1)");
		$db->where("date_delete IS NULL");
		$db->where("pos_session_id = '".$pos_session->id."'");
		// avoid closing cashier with open parked orders
		if ($db->run()) {
			redirect_to('terminal/index?error=parked_orders');
		}

		if ($this->post) {
			$this->post['protocol'] = crypto_rand_secure(100000, 999999);

			// sums total counted by employee
			$total_counted = 0;
			// sum credit cards
			foreach ($this->post['credit_card'] as $key => $value) {
				$total_counted += (double)$value;
			}
			// sum money on drawer
			$total_counted += (double)$this->post['amount_counted'];

			// calculates amount of money received by the system
			$amount_received = 0;
			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("SUM(OP.amount_paid) AS total");
			$db->from("Order_payments", "OP");
			$db->join("Orders O", "O.id = OP.order_id");
			$db->where("O.pos_session_id = '".$pos_session->id."'");
			$db->where("OP.payment_method_id = '1'");// money only
			if ($result = $db->run()) {
				foreach ($result as $key => $value) {
					$amount_received += (double)$value['total'];
				}
			}
			// sums start money to amount received
			$amount_received += (double)$pos_session->cash_fund;

			// calculates amount of credit card received by the system
			$amount_credit_card = 0;
			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("SUM(OP.amount_paid) AS total");
			$db->from("Order_payments", "OP");
			$db->join("Orders O", "O.id = OP.order_id");
			$db->join("Payment_methods PM", "PM.id = OP.payment_method_id");
			$db->where("O.pos_session_id = '".$pos_session->id."'");
			$db->where("PM.show_on_closure = '1'");// credit card only
			if ($result = $db->run()) {
				foreach ($result as $key => $value) {
					$amount_credit_card += (double)$value['total'];
				}
			}

			$closure_model = $this->load()->model('Pos_Session_Closures');
			$data = [
				'user_id' => $_SESSION['app']['user']['id'],
				'pos_session_id' => $pos_session->id,
				'protocol' => crypto_rand_secure(100000, 999999),
				'date' => now(),
				'amount_counted' => $total_counted,
				'amount_received' => $amount_received,
				'amount_credit_card' => $amount_credit_card,
			];
			// _dump($data, 1);

			$closure_model->set_values($data);
			if ($closure_model->create()) {
				$pos_session->set('end_date', now());
				$pos_session->update();
				redirect_to('session/logout');
			}
		}

		// get payments done with card
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("PM.bbsid, PM.name, SUM(OP.amount) AS total");
		$db->from("Order_payments", "OP");
		$db->join("Payment_methods PM", "PM.id = OP.payment_method_id");
		$db->join("Orders O", "O.id = OP.order_id");
		$db->where("O.pos_session_id = '".$pos_session->id."'");
		$db->where("PM.show_on_closure = '1'");
		$db->group("PM.id");
		$db->order("PM.id", "ASC");
		$payment_report = $db->run();

		$payment_methods = $this->load()->model('Payment_methods')->retrieve(['show_on_closure'=>'1']);

		$params = [
			'pos' => $pos,
			'user' => $user,
			'payment_methods' => $payment_methods,
			'payment_report' => $payment_report
		];

		$this->load()->view('terminal/close', $params);
	}

	public function show($order_id) {
		auth('yes');
		global $CONFIG;

		// order details
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("O.*, P.name AS client_name, POS.nickname, PM.name AS payment_method_name, U.login");
		$db->from("Orders", "O");
		$db->join("Pos_sessions PS", "PS.id = O.pos_session_id");
		$db->join("Pos POS", "POS.id = PS.pos_id");
		$db->join("People P", "P.id = O.person_id");
		$db->join("Users U", "U.id = O.user_id");
		$db->l_join("Order_payments OP", "OP.order_id = O.id");
		$db->l_join("Payment_methods PM", "PM.id = OP.payment_method_id");
		$db->where("O.id = '".$order_id."'");
		if (!$order = $db->run()) {
			die('Order not found');
		}

		// items
		$db->reset();
		$db->select("OL.*, P.name AS product_name, P.product_number, S.name AS size_name, C.name AS color_name");
		$db->from("Order_lines", "OL");
		$db->join("Products P", "P.id = OL.product_id");
		$db->l_join("Size_colors SC", "SC.id = OL.size_color_id");
		$db->l_join("Sizes S", "S.id = SC.size_id");
		$db->l_join("Colors C", "C.id = SC.color_id");
		$db->where("OL.order_id = '".$order_id."'");
		$items = $db->run();

		// payments
		$db->reset();
		$db->select("OP.*, PM.name AS payment_method_name");
		$db->from("Order_payments", "OP");
		$db->join("Payment_methods PM", "PM.id = OP.payment_method_id");
		$db->where("OP.order_id = '".$order_id."'");
		$payments = $db->run();

		$params = [
			'order' => $order[0],
			'items' => $items,
			'payments' => $payments,
			'order_types' => $CONFIG['order_types']
		];

		$this->load()->view('terminal/show', $params);
	}

	public function payment_methods() {
		auth('yes');
		global $__post;
		global $CONFIG;

		if ($__post) {
			// update stuff
			if (count($__post['update'])) {
				$payment_method_model = $this->load()->model('Payment_methods');
				foreach ($__post['update'] as $key => $value) {
					if (!isset($value['show_on_closure'])) {
						$value['show_on_closure'] = '0';
					}
					$obj = $payment_method_model->get_by_id($key);
					$obj->set_values($value);
					$obj->update();
				}
			}
			// register
			if (count($__post['new']) && ($__post['new'][1]['name'] != '')) {
				$payment_method_model = $this->load()->model('Payment_methods');
				foreach ($__post['new'] as $key => $value) {
					if (!isset($value['show_on_closure'])) {
						$value['show_on_closure'] = '0';
					}
					$payment_method_model->set_values($value);
					$payment_method_model->create();
				}
			}

			$CONFIG['msg']['success'][] = $this->lang['success'];
		}

		$params = [
			'payment_methods' => $this->load()->model('Payment_methods')->retrieve('all')
		];

		$this->load()->view('terminal/payment_methods', $params);
	}

	public function receipts() {
		auth('yes');
		global $__post;
		global $CONFIG;

		$pos = $this->getPos();
		$settings = $this->load()->model('Pos_Settings_Receipts')->retrieve(['pos_id'=>$pos->id]);

		if (isset($__post['settings'])) {
			// update stuff
			if (!isset($__post['settings']['print_two_lines'])) {
				$__post['settings']['print_two_lines'] = '0';
			}
			$settings_model = $this->load()->model('Pos_Settings_Receipts');
			foreach ($__post['settings'] as $key => $value) {
				if (isset($settings[$key])) {
					$obj = $this->load()->model('Pos_Settings_Receipts')->get_by(['data_type'=>$key, 'pos_id'=>$pos->id]);
					$obj->set('data_value', $value);
					$obj->update();
				}
				else {
					$data = [
						'pos_id' => $pos->id,
						'data_type' => $key,
						'data_value' => $value
					];
					$settings_model->set_values($data);
					$settings_model->create();
				}
			}

			$CONFIG['msg']['success'][] = $this->lang['success'];
		}

		$settings = [];
		if ($settings_list = $this->load()->model('Pos_Settings_Receipts')->retrieve(['pos_id'=>$pos->id])) {
			foreach ($settings_list as $key => $value) {
				$settings[$value['data_type']] = $value['data_value'];
			}
		}

		$params = [
			'settings' => $settings
		];

		$this->load()->view('terminal/receipts', $params);
	}
}