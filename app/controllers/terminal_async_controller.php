<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

use Application\Helpers\Http as Http;

class Terminal_Async_Controller extends Core\App_Controller {

	private function getPos() {
		global $CONFIG;
		return $this->load()->model('Pos')->get_by(['pos_number'=>$CONFIG['device_id']]);
	}

	private function getPosSession() {
		global $CONFIG;
		$pos = $this->getPos();

		return $this->load()->model('Pos_session')->get_by(['pos_id'=>$pos->id, 'end_date'=>'null']);
	}

	private function getPromotionalPrices() {
		auth('yes');
		$db = new Sql(get_pdo());

		// get promotional prices
		$promotional = [];
		$today = date('Y-m-d H:i:s');
		$db->array_only = true;
		$db->select("*");
		$db->from("Products_promotions");
		$db->where("(initial_date < '{$today}' AND final_date > '{$today}')");
		$db->where("active = '1'");
		if ($result = $db->run()) {
			foreach ($result as $key => $value) {
				$promotional[$value['product_id']] = $value['gross_price'];
			}
		}

		return $promotional;
	}

	/**
	* Search products based on certain criteria
	*
	* This method returns a product list to the main POS screen
	* being called by a search parameter that may represent several
	* database fields such as name or ean.
	*
	* @param string $term A search term send via post
	*
	* @return string
	*/
	public function searchProducts() {
		auth('yes');
		$term = $_REQUEST['term'];

		$promotional = $this->getPromotionalPrices();

		// get product
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("P.*, SC.id AS size_color_id, SC.price AS size_color_price, S.name AS size_name, C.name AS color_name");
		$db->from("Product", "P");
		$db->l_join("Size_Color SC", "SC.product_id = P.id");
		$db->l_join("Size S", "S.id = SC.size_id");
		$db->l_join("Color C", "C.id = SC.color_id");
		$db->where("(P.name LIKE '%".$term."%' OR barcode = '".$term."' OR product_number = '".$term."')");
		$db->where("P.type_product = '".$_REQUEST['type_product']."'");
		$db->group("P.id");
		$db->group("SC.id");
		$db->limit(100);
		if ($result = $db->run()) {
			foreach ($result as $key => $value) {
				// check size and color price
				if (array_key_exists($value['id'], $promotional)) {
					$result[$key]['price'] = $promotional[$value['id']];
				}
				else if ($value['size_color_price']) {
					$result[$key]['price'] = $value['size_color_price'];
				}
			}
			$this->load()->view('terminal/async/search_products', ['products'=>$result]);
		}
		else {
			echo '<tr><td colspan="5"><em>Sem resultados</em></td></tr>';
		}
		
	}

	/**
	* Select a specific product by id
	*
	* Return product details based on a specific ID
	*
	* @param string $product_id product identifier
	* @param string $size_color_id size_color identifier (optional)
	* @param float $qty product quantity
	* @param bool $order_line_id defines if the product is being retrieved from the database or added to the order
	*
	* @return string html table line with product details
	*/
	public function selectProduct($product_id, $size_color_id=null, $qty=null, $order_line_id=false, $amount=false, $internal=false) {
		auth('yes');
		$reload_order = false;

		if ($order_line_id == 'false') {
			$order_line_id = false;
		}

		// check if this item is already into the order
		$params = [
			'order_id' => $_SESSION['app']['order']['id'], 
			'product_id' => $product_id
		];
		if ($size_color_id) {
			$params['size_color_id'] = $size_color_id;
		}
		if (!$order_line_id && ($order_line = $this->load()->model('Order_lines')->get_by($params))) {
			$qty = $order_line->qty + 1;
			$order_line->set('qty', $qty);
			$order_line->update();
			$order_line_id = $order_line->id;
			$reload_order = true;
		}

		// get promotional prices
		$promotional = $this->getPromotionalPrices();

		// get product
		$db = new Sql(get_pdo());
		$db->select("P.*, P.price AS value, P.price AS actual_value, SC.id AS size_color_id, SC.price AS size_color_price, S.name AS size_name, C.name AS color_name, T.percent AS mva");
		$db->from("Product", "P");
		$db->l_join("Size_Color SC", "SC.product_id = P.id");
		$db->l_join("Size S", "S.id = SC.size_id");
		$db->l_join("Color C", "C.id = SC.color_id");
		$db->l_join("Taxes T", "T.id = P.tax_id");
		$db->where("P.id = '".$product_id."'");
		if ($size_color_id) {
			$db->where("SC.id = '".$size_color_id."'");
		}
		$db->limit(1);
		if (!$result = $db->run()) {
			echo 'product_not_found';
			return false;
		}
		// _dump($result);
		$result->qty = 1;
		// get size and color price
		if (array_key_exists($result->id, $promotional)) {
			$result->value = $promotional[$result->id];
			$result->actual_value = $promotional[$result->id];
		}
		else if ($result->size_color_price) {
			$result->value = $result->size_color_price;
			$result->actual_value = $result->size_color_price;
		}

		if (!$order_line_id) {
			// insert products into order_lines
			$order_line_model = $this->load()->model('Order_Lines');

			$tax = 0;
			$result->discount = 0;
			if ($result->mva) {
				$tax = $result->value * ($result->mva/100);
			}
			$params = [
				'order_id' => $_SESSION['app']['order']['id'],
				'product_id' => $result->id,
				'size_color_id' => $result->size_color_id,
				'user_id' => $_SESSION['app']['user']['id'],
				'value' => $result->value,
				'discount' => $result->discount,
				'actual_value' => $result->value,
				'tax' => $tax,
				'qty' => 1,
				'active' => 1
			];

			if ($amount) {
				$params['value'] = $amount;
				$params['actual_value'] = $amount;
			}
			// _dump($params);
			$order_line_model->set_values($params);
			$order_line_id = $order_line_model->create();
			$order_line = $this->load()->model('Order_Lines')->get_by_id($order_line_id);
		}
		else {
			// get price from order_line
			$order_line = $this->load()->model('Order_Lines')->get_by_id($order_line_id);
			$result->value = $order_line->value;
			$result->qty = $order_line->qty;
			$result->actual_value = $order_line->actual_value;
			$result->discount = $order_line->discount;
			$result->tax = $order_line->tax;
			// _dump($result);
		}
		// $this->updateOrderTotal(true);

		if (!$internal) {
			$this->load()->view('terminal/async/select_product', ['product'=>$result, 'order_line_id'=>$order_line_id, 'order_line'=>$order_line, 'reload_order'=>$reload_order]);
		}
	}

	/**
	* Initiates an empty order to be used by the terminal
	* Retrieves stored order to the terminal screen
	*
	* @param
	*
	* @return String json object with request status and order data.
	* may also return error code details, if any.
	*/
	public function initOrder() {
		header("Content-type: application/json");

		$order_model = $this->load()->model('Orders');

		// se não houver nenhuma ordem na sessão, cria-se uma nova
		if (!isset($_SESSION['app']['order']['id'])) {

			$params = [
				'person_id' => '1',// default client
				'user_id' => $_SESSION['app']['user']['id'],
				'pos_session_id' => $_GET['pos_session_id'],
				'in_use' => 1,
				'status' => 0,
				'order_type' => 0,
				'active' => 1
			];

			$order_model->set_values($params);
			if ($id = $order_model->create()) {
				$order = $order_model->get_by_id($id);
				// gets employee and customer data
				$user = $this->load()->model('Users')->get_by_id($_SESSION['app']['user']['id']);
				$person = $this->load()->model('People')->get_by_id(1);
				$order->employee_name = $user->login;
				$order->client_name = $person->name;
				// inserts order_id into session
				$_SESSION['app']['order']['id'] = $id;
				return $this->returnJson(200, $order);
			}
			else {
				return $this->returnJson(500);
			}
		}
		else {
			if ($order = $order_model->get_by_id($_SESSION['app']['order']['id'])) {
				// gets employee and customer data
				$user = $this->load()->model('Users')->get_by_id($order->user_id);
				$person = $this->load()->model('People')->get_by_id($order->person_id);
				$order->employee_name = $user->login;
				$order->client_name = $person->name;
				// retrieve order_lines, if any
				$order_line_model = $this->load()->model('Order_Lines');
				$order_lines = $order_line_model->retrieve(['order_id'=>$order->id]);
				return $this->returnJson(200, $order, $order_lines);
			}
			else {
				return $this->returnJson(500);
			}
		}
	}

	/**
	* Updates order total
	* Gets order_id from session
	*
	* @param bool $internal Defines if method returns json or html, for internal use
	*
	* @return String json object with request status and order data
	* may also return error code details, if any.
	*/
	public function updateOrderTotal($internal=false) {
		if (!$internal) {
			header("Content-type: application/json");
		}
		$total = $vat = 0.0;
		if ($order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id'])) {
			// recupera ordelines, caso haja
			if ($order_lines = $this->load()->model('Order_Lines')->retrieve(['order_id'=>$order->id])) {
				$total = $vat = 0.0;
				foreach ($order_lines as $key => $value) {
					$total += $value['actual_value'];
					$vat += $value['tax'];
				}
			}
			//$total += $vat;
			$order->set('amount', $total);
			$order->set('vat', $vat);
			$order->update();
			$amount_outstanding = $total;
			$amount_paid = 0;
			if ($payments = $this->load()->model('Order_Payments')->retrieve(['order_id'=>$order->id])) {
				foreach ($payments as $key => $value) {
					$amount_paid += $value['amount_paid'];
				}
				$amount_outstanding -= $amount_paid;
			}
			$order->set('amount_outstanding', $amount_outstanding);
			$order->set('amount_paid', $amount_paid);
			if ($total > 0 && $amount_outstanding <= 0) {
				$order->set('status', '2');// paid
			}
			else if ($total > 0 && $amount_paid > 0) {
				$order->set('status', '1');// part payment
			}
			$order->update();
			if (!$internal) {
				return $this->returnJson(200, $order);
			}
		}
		else {
			return $this->returnJson(404);
		}
	}

	public function selectEmployee() {
		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users
		];

		$this->load()->view('terminal/async/select_employee', $params);
	}

	public function selectClient() {
		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users
		];

		$this->load()->view('terminal/async/select_client', $params);
	}

	public function addClient() {
		$users = $this->load()->model('Users')->retrieve('all');

		$params = [
			'users' => $users
		];

		$this->load()->view('terminal/async/add_client', $params);
	}

	public function registerClient() {
		header("Content-type: application/json");

		$person_model = $this->load()->model('People');
		$person_model->set_values($_POST);

		if ($id = $person_model->create()) {
			return $this->returnJson(200, $_POST, $id);
		}
		else {
			return $this->returnJson(500, $_POST);
		}
		
	}

	public function searchClients() {
		header("Content-type: application/json");

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("id, name, document_number, personal_number");
		$db->from("People");
		$db->where("(name LIKE '".$_GET['q']."%' OR document_number LIKE '".$_GET['q']."%')");
		$db->order("name", "ASC");
		$db->limit(20);
		$result = $db->run();

		$filterArray = function($item){
			return array_values($item);
		};

		$data = [];
		if ($result) {
			$data = array_map($filterArray, $result);
		}

		return $this->returnJson(200, $data, $_GET);
	}

	public function updateProductLine() {
		header("Content-type: application/json");
		// only post data is allowed
        if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
            return $this->returnJson(405, $__server['REQUEST_METHOD']);
        }

		if (!$order_line = $this->load()->model('Order_Lines')->get_by_id($_POST['order_line_id'])) {
			return $this->returnJson(404, $_POST);
		}

		switch ($_POST['op']) {
			case 'qty':
				$qty = (float)$_POST['qty'];
				$order_line->qty = $qty;
				$order_line->actual_value = ($order_line->value * $qty) - $order_line->discount;
				break;
			case 'unit_price':
				$unit_price = (float)$_POST['unit_price'];
				$order_line->value = $unit_price;
				$order_line->actual_value = ($order_line->value * $order_line->qty) - $order_line->discount;
				break;
			case 'discount':
				$discount = (float)$_POST['discount'];
				$order_line->discount = $discount;
				$order_line->actual_value = ($order_line->value * $order_line->qty) - $discount;
				break;
			case 'vat':
				$vat = (float)$_POST['vat'];
				$order_line->tax = $vat;
				break;
			case 'delete':
				if ($serial = $this->load()->model('Product_serial_numbers')->get_by(['order_line_id'=>$order_line->id, 'product_id'=>$order_line->product_id])) {
					$serial->delete();
				}
				$order_line->delete();
				break;
			default:
				break;
		}

		$order_line->update();

		$this->updateOrderTotal(true);

		return $this->returnJson(200, $order_line);
	}

	public function paymentWindow() {
		$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);

		$order_lines = $this->load()->model('Order_Lines')->get_by(['order_id'=>$_SESSION['app']['order']['id']]);

		$payment_methods = $this->load()->model('Payment_Methods')->retrieve('all');

		$payment_model = $this->load()->model('Order_Payments');
		$payment_model->join()->prefix('OP');
		$payment_model->join()->tables(['inner'=>['payment_methods'=>['PM.id'=>'OP.payment_method_id']]]);
		$payment_model->join()->fields(['PM.name'=>'payment_method_name']);
		$payments = $payment_model->retrieve(['order_id'=>$order->id]);

		$bills = [1000, 500, 200, 100, 50];

		$params = [
			'order' => $order,
			'order_lines' => $order_lines,
			'payment_methods' => $payment_methods,
			'payments' => $payments,
			'bills' => $bills
		];
		$this->load()->view('terminal/async/payment_window', $params);
	}

	public function orderLineDetails() {
		// only post data is allowed
        if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
            return $this->returnJson(405, $__server['REQUEST_METHOD']);
        }

		if (!$order_line = $this->load()->model('Order_Lines')->get_by_id($_POST['id'])) {
			echo 'Produto não encontrado.';
		}

		$users = $this->load()->model('Users')->retrieve('all', ['order'=>['login'=>'ASC']]);
		$product = $this->load()->model('Products')->get_by_id($order_line->product_id);
		$serial = $this->load()->model('Product_serial_numbers')->get_by(['order_line_id'=>$order_line->id, 'product_id'=>$order_line->product_id]);

		$params = [
			'order_line' => $order_line,
			'users' => $users,
			'product' => $product,
			'serial' => $serial
		];

		$this->load()->view('terminal/async/order_line_details', $params);
	}

	public function updateOrderLineDetails() {
		header("Content-type: application/json");

		if ($_POST) {
			if (!$order_line = $this->load()->model('Order_Lines')->get_by_id($_POST['order_line_id'])) {
				return $this->returnJson(404, $_POST);
			}

			if (!isset($_POST['is_gift'])) {
				$_POST['is_gift'] = 0;
			}

			// set serial number
			if (isset($_POST['serial']) && $_POST['serial'] != '') {
				$serial_model = $this->load()->model('Product_serial_numbers');
				// update serial number for current card purchase (new card being purchased right now)
				if ($serial = $serial_model->get_by(['order_line_id'=>$order_line->id, 'product_id'=>$order_line->product_id])) {
					$serial->set('serial', $_POST['serial']);
					$serial->update();
				}
				// create new card
				else {
					$params = [
						'order_line_id' => $order_line->id,
						'product_id' => $order_line->product_id,
						'serial' => $_POST['serial']
					];
					$serial_model->set_values($params);
					$serial_model->create();
				}
				unset($_POST['serial']);
			}

			$order_line->set_values($_POST);
			// update order line total
			$total = (float)$order_line->value * (float)$order_line->qty;
			$order_line->set('actual_value', $total);

			$order_line->update();
		}

		return $this->returnJson(200, $_POST);
	}

	public function rechargeGiftCard() {
		$serial_model = $this->load()->model('Product_serial_numbers');

		// get orderline
		if (!$order_line = $this->load()->model('Order_Lines')->get_by_id($_POST['order_line_id'])) {
			return $this->returnJson(404, $_POST);
		}
		// get gift card
		// records with gift card product_id and serial number
		if (!$card = $serial_model->retrieve(['product_id'=>$order_line->product_id, 'serial'=>$_POST['serial']])) {
			return $this->returnJson(404);
		}

		$params = [
			'order_line_id' => $order_line->id,
			'product_id' => $order_line->product_id,
			'serial' => $_POST['serial']
		];
		$serial_model->set_values($params);
		if ($serial_model->create()) {
			return $this->returnJson(200);
		}
	}

	// check if gift card already exists upon sale
	public function checkGiftCard() {
		header("Content-type: application/json");

		if (!$order_line = $this->load()->model('Order_Lines')->get_by_id($_GET['order_line_id'])) {
			return $this->returnJson(404, $_GET);
		}

		if ($card = $this->load()->model('Product_serial_numbers')->retrieve(['product_id'=>$order_line->product_id, 'serial'=>$_GET['serial']])) {
			return $this->returnJson(200);
		}

		return $this->returnJson(404);
	}

	// TODO: verify gift card
	public function verifyGiftCard() {
		header("Content-type: application/json");

		$code = $_POST['code'];

		// get card from serial table
		$db = new Sql(get_pdo());
		$db->select("*");
		$db->from("Product_serial_numbers", "PSN");
		// $db->join();
	}

	public function registerPaymentOld() {
		header("Content-type: application/json");

		$payment_model = $this->load()->model('Order_Payments');

		// $data = json_decode($_POST['data'], true);
		$data = [
			'order_id' => $_SESSION['app']['order']['id'],
			'payment_method_id' => $_POST['payment_method_id'],
			'amount' => (double)$_POST['amount'],
			'change' => (double)$_POST['change'],
			'amount_paid' => ((double)$_POST['amount'] - (double)$_POST['change']),
			'active' => '1'
		];

		$payment_model->set_values($data);

		if ($id = $payment_model->create()) {
			// credit order
			if ($_POST['payment_method_id'] == '2000') {
				$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);
				$order->set('order_type', '1');
				$order->update();
			}
			return $this->returnJson(200, $id);
		}

		return $this->returnJson(500, $_POST);
	}

	public function registerPayment() {
		header("Content-type: application/json");

		if (!isset($_POST['amount']) || !isset($_POST['payment_method'])) {
			return $this->returnJson(500, $_POST);
		}

		if (!$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id'])) {
			return $this->returnJson(500, $_POST);
		}

		foreach ($_POST['amount'] as $key => $value) {
			$payment_model = $this->load()->model('Order_Payments');

			// randomize payment method id for test purposes

			if ($_POST['payment_method'][$key] == 2) {
				$_POST['payment_method'][$key] = rand(2, 7);
			}

			$payment_data = [
				'order_id' => $_SESSION['app']['order']['id'],
				'payment_method_id' => $_POST['payment_method'][$key],
				'amount' => (double)$value,
				'amount_paid' => (double)$value,
				'active' => '1'
			];

			$payment_model->set_values($payment_data);

			if ($id = $payment_model->create()) {
				// credit order
				if ($_POST['payment_method'][$key] == '2000') {
					$order->set('order_type', '1');
					$order->update();
				}
			}
		}

		$amount_paid = 0;
		if ($payments = $this->load()->model('Order_Payments')->retrieve(['order_id'=>$order->id])) {
			foreach ($payments as $key => $value) {
				$amount_paid += $value['amount_paid'];
			}
		}

		// update order total
		$amount_outstanding = (double)$order->amount - $amount_paid;

		$order->set('amount_paid', $amount_paid);
		$order->set('amount_outstanding', $amount_outstanding);
		$order->update();

		return $this->returnJson(200, ['amount_outstanding'=>(double)$amount_outstanding]);
	}

	/**
	* Updates assorted fields on order table
	*
	* @param String $field Field name (via post)
	* @param String $value Value for specific field (via post)
	*
	* @return String json object with request status
	*/
	public function updateOrder() {
		global $__post;

		header("Content-type: application/json");
		$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);

		foreach ($__post as $key => $value) {
			$order->set($key, $value);
		}
		
		if ($order->update()) {
			return $this->returnJson(200, $__post);
		}
	}

	/**
	* Updates assorted fields on order table
	*
	* @param String $field Field name (via post)
	* @param String $value Value for specific field (via post)
	*
	* @return String json object with request status
	*/
	public function orderDetails() {
		$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);

		$params = [
			'order' => $order
		];

		$this->load()->view('terminal/async/order_details', $params);
	}

	/**
	* Park orders or join order lines to previously parked orders
	*
	* @return String html page to used into modal window
	*/
	public function parkOrder() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$pos_session = $this->getPosSession();

			// check if parked order already exists
			if (isset($_GET['verify'])) {
				$db = new Sql(get_pdo());
				$db->array_only = false;
				$db->select("*");
				$db->from("Orders");
				$db->where("extra_info = '".$__post['description']."'");
				$db->where("pos_session_id = '".$pos_session->id."'");
				$db->where("order_type = '0'");

				if ($result = $db->run()) {
					return $this->returnJson(200, $result);
				}
				else {
					return $this->returnJson(404);
				}
			}
			// actually park the order
			else {
				// get order from session
				$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id']);
				// check if order lines needs to be joined to an existing order
				if (isset($__post['order_id'])) {
					if ($order_lines = $this->load()->model('Order_Lines')->retrieve(['order_id'=>$_SESSION['app']['order']['id']])) {
						foreach ($order_lines as $item) {
							$order_line = $this->load()->model('Order_Lines')->get_by_id($item['id']);
							$order_line->set('order_id', $__post['order_id']);
							$order_line->update();
						}
						// deletes current order from the database
						$order->set('active', '0');
						return $this->returnJson(200);
					}
				}
				else {
					// updates order extra_info
					$order->set('extra_info', $__post['description']);
					if ($order->update()) {
						return $this->returnJson(200, $order);
					}
					else {
						return $this->returnJson(500, $order);
					}
				}
			}

			return $this->returnJson(200, $result);
		}
		else {
			$params = [];
			$this->load()->view('terminal/async/park_order', $params);
		}
	}

	/**
	* Search parked orders to be load into the terminal screen
	*
	* @return String html page to used into modal window
	*/
	public function searchParkedOrder() {
		global $__post;

		$pos_session = $this->getPosSession();

		if ($__post) {
			header("Content-type: application/json");

			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("*");
			$db->from("Orders");
			$db->where("extra_info LIKE '%".$__post['description']."%'");
			$db->where("status != '2'");// can't be fully paid

			$result = $db->run();

			return $this->returnJson(200, $result);
		}
		else {
			$db = new Sql(get_pdo());
			$db->array_only = true;
			$db->select("*");
			$db->from("Orders");
			$db->where("pos_session_id = '".$pos_session->id."'");
			$db->where("status != '2'");// can't be fully paid
			$db->where("amount > 0");
			$params = [
				'orders' => $db->run()
			];
			$this->load()->view('terminal/async/search_parked_order', $params);
		}
	}

	// register deposits and cashouts from the POS
	public function moneyMovement() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			if ($__post['type'] == 'cashout') {
				$__post['amount'] *= -1;
			}

			$pos_session = $this->getPosSession();

			$__post['pos_session_id'] = $pos_session->id;

			$model = $this->load()->model('Pos_session_money_movement');
			$model->set_values($__post);
			if ($model->create()) {
				return $this->returnJson(200);
			}
			return $this->returnJson(500);
		}
		else {
			$params = [];
			$this->load()->view('terminal/async/money_movement', $params);
		}
	}

	// register discount for an order
	public function orderDiscount() {
		global $__post;

		if (!$order = $this->load()->model('Orders')->get_by_id($_SESSION['app']['order']['id'])) {
			return $this->returnJson(500);
		}

		if ($__post) {
			header("Content-type: application/json");

			$discount_id = $this->meta['product_discount_id'];

			// get discount if percentage
			if (strstr($__post['discount'], '%')) {
				$d = str_replace('%', '', $__post['discount']);
				$__post['discount'] = (double)$order->amount * ((double)$d / 100);
			}

			$__post['discount'] *= -1;

			$this->selectProduct($discount_id, false, 1, false, $__post['discount'], true);

			return $this->returnJson(200);
		}
		else {
			$params = [
				'order' => $order
			];
			$this->load()->view('terminal/async/order_discount', $params);
		}
	}

	public function removeFavoriteButton() {
		global $__post;
		header("Content-type: application/json");

		if (isset($__post['id'])) {
			if ($button = $this->load()->model('Favorite_Buttons')->get_by_id($__post['id'])) {
				$db = new Sql(get_pdo());
				$db->delete();
				$db->from("Favorite_Buttons");
				$db->where("id = '".$button->id."'");
				if ($db->run()) {
					return $this->returnJson(200);
				}
				else {
					return $this->returnJson(500);
				}
			}
			else {
				return $this->returnJson(404);
			}
		}

		return $this->returnJson(500);
	}

	public function addTab() {
		global $__post;
		header("Content-type: application/json");

		if (isset($__post['name'])) {
			$tab_model = $this->load()->model('Favorite_Button_Tabs');
			$tab_model->set_values(['name' => $__post['name']]);
			if ($id = $tab_model->create()) {
				return $this->returnJson(200, $id);
			}
		}
	}

	public function printOrder($order_id=false) {
		header("Content-type: application/json");

		// return $this->returnJson(200);

		$pos = $this->getPos();

		$meta_model = $this->load()->model('Meta');

		if (!$server_print = $meta_model->get_by(['data_type'=>'server_print'])) {
			return $this->returnJson(601);// print server not found
		}

		$printer_url = 'http://'.$server_print->data_value.'/pos_printer/receipt.php';

		$order_id = $order_id ? $order_id : $_SESSION['app']['order']['id'];

		if (!$order = $this->load()->model('Orders')->get_by_id($order_id)) {
			die('Order not found');
		}

		$lines = $vat_lines = [];
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("OL.*, P.name AS product_name, S.name AS size_name, C.name AS color_name, T.percent, P.tax_id, T.name AS tax_name");
		$db->from("Order_lines", "OL");
		$db->join("Products P", "P.id = OL.product_id");
		$db->l_join("Size_colors SC", "SC.id = OL.size_color_id");
		$db->l_join("Sizes S", "S.id = SC.size_id");
		$db->l_join("Colors C", "C.id = SC.color_id");
		$db->l_join("Taxes T", "T.id = P.tax_id");
		$db->where("OL.order_id = '".$order->id."'");
		if ($result = $db->run()) {
			foreach ($result as $key => $value) {
				$name = $value['product_name'];
				if ($value['color_name']) {
					$name .= ' '.$value['color_name'];
				}
				if ($value['size_name']) {
					$name .= ' '.$value['size_name'];
				}
				$lines[] = [
					'name' => $name,
					'qty' => $value['qty'],
					'price' => $value['value'],
					'total_price' => $value['actual_value'],
					'tax' => $value['tax'],
					'tax_id' => @$value['tax_id']
				];

				if (!isset($value['tax_id'])) {
					$value['tax_id'] = 0;
					$value['tax_name'] = 'MVA FRITT';
					$value['percent'] = 0;
				}
				// save mva details
				if (!isset($vat[$value['tax_id']])) {
					$vat_lines[$value['tax_id']] = [
						'name' => $value['tax_name'],
						'percent' => $value['percent'],
						'vat' => 0,
						'amount' => 0
					];
				}
				$vat_lines[$value['tax_id']]['vat'] += $value['tax'];
				$vat_lines[$value['tax_id']]['amount'] += $value['actual_value'];
			}
		}

		// get local settings
		$settings = json_decode(file_get_contents(DOCROOT.'/app/config/config.json'), true);
		$url = $settings['sync']['server'].'/getcompanyData';
		// token located at config.json
		$fields = [
			'token' => $settings['sync']['token']
		];
		// get company ata from web service
		$company_data = json_decode(httpPOST($url, $fields, true), true);

		$pos_settings = [];
		if ($pos_receipt_settings = $this->load()->model('Pos_Settings_Receipt')->retrieve(['pos_id' => $pos->id])) {
			foreach ($pos_receipt_settings as $key => $value) {
				$pos_settings[$value['data_type']] = $value['data_value'];
			}
		}

		$data = [
			'printer' => [
				'type' => $meta_model->get_by(['data_type'=>'terminal_printer_type'])->data_value,
				'name' => $meta_model->get_by(['data_type'=>'terminal_printer_ip'])->data_value
			],
			'order' => (array)$order,
			'order_lines' => $lines,
			'vat_lines' => $vat_lines,
			'company_data' => $company_data['items'],
			'pos_settings' => $pos_settings
		];

		// _dump($data, 1);

		// echo $url;

		$http = new Http($printer_url);
		$http->setMethod('post');
		$http->setContentType('application/json');
		$http->setFields($data);
		$return = $http->run();

		if (strstr($return, '200 OK')) {
			return $this->returnJson(200, $return);
		}
		else {
			return $this->returnJson(500, $return);
		}

	}
}