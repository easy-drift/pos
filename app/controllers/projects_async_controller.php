<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Projects_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$project_model = $this->load()->model('Projects');
			$project_model->set_values($__post);

			if ($id = $project_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('projects/async/add');
	}

	public function update($id) {
		global $__post;

		$project = $this->load()->model('Projects')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$project->set_values($__post);

			if ($id = $project->update()) {
				return $this->returnJson(200, $__post, $project);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'project' => $project
		];

		$this->load()->view('projects/async/update', $params);
	}
}