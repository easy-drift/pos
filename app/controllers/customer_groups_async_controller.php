<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Customer_Groups_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$customer_group_model = $this->load()->model('Customer_groups');
			$customer_group_model->set_values($__post);

			if ($id = $customer_group_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('customers/async/customer_group_add');
	}

	public function update($id) {
		global $__post;

		$customer_group = $this->load()->model('customer_groups')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$customer_group->set_values($__post);

			if ($id = $customer_group->update()) {
				return $this->returnJson(200, $__post, $customer_group);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'customer_group' => $customer_group
		];

		$this->load()->view('customers/async/customer_group_update', $params);
	}
}