<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Products_Async_Controller extends Core\App_Controller {
	public function addPromotionalPrice($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';//$this->index();
		}

		if ($_POST) {
			header("Content-type: application/json");

			$promotion_model = $this->load()->model('Products_promotions');
			$promotion_model->set_values($_POST);

			if ($id = $promotion_model->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$params = [
			'product' => $product
		];

		$this->load()->view('products/async/add_promotional_price', $params);
	}

	public function loadPromotionalPrices($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$promotions = $this->load()->model('Products_promotions')->retrieve(['product_id' => $product_id]);

		$params = [
			'promotions' => $promotions
		];

		$this->load()->view('products/async/load_promotional_prices', $params);
	}

	public function removePromotionalPrice() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			header("Content-type: application/json");

			$promotion = $this->load()->model('Products_promotions')->get_by_id($_POST['id']);

			if ($promotion->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);

	}

	public function addProductGroup($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';//$this->index();
		}

		if ($_POST) {
			header("Content-type: application/json");

			$group_model = $this->load()->model('Product_product_groups');
			$group_model->set_values($_POST);

			if ($id = $group_model->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$groups = $this->load()->model('Product_groups')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'product' => $product,
			'groups' => $groups
		];

		$this->load()->view('products/async/add_product_group', $params);
	}

	public function loadProductGroups($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$tables_to_join = [
			'inner' => [
				'product_groups' => 'PG.id'
			]
		];
		$group_model = $this->load()->model('Product_product_group');
		$group_model->join()->prefix('PGG');
		$group_model->join()->tables($tables_to_join);
		$group_model->join()->fields(['PG.name'=>'group_name']);
		$groups = $group_model->retrieve(['product_id' => $product_id]);

		$params = [
			'groups' => $groups
		];

		$this->load()->view('products/async/load_product_groups', $params);
	}

	public function removeProductGroup() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			header("Content-type: application/json");

			$group = $this->load()->model('Product_product_groups')->get_by_id($_POST['id']);

			if ($group->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);

	}

	public function addImage($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			header("Content-type: application/json");
			return $this->returnJson(404);
		}

		if ($_POST) {
			header("Content-type: application/json");

			// $_POST['path'] = base64_encode(addslashes(file_get_contents($_FILES['path']['tmp_name'])));

			$imageFileType = strtolower(pathinfo($_FILES['path']['name'], PATHINFO_EXTENSION));

			$image_base64 = base64_encode(file_get_contents($_FILES['path']['tmp_name']) );
    		$_POST['path'] = 'data:image/'.$imageFileType.';base64,'.$image_base64;

			$image = $this->load()->model('Product_images');
			$image->set_values($_POST);

			if (isset($_POST['main']) && $_POST['main'] == '1') {
				$db = new Sql(get_pdo());
				$db->update('Product_images');
				$db->set('main', '0');
				$db->where("product_id = '".$product->id."'");
				$db->run();
			}

			if ($id = $image->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$params = [
			'product' => $product
		];

		$this->load()->view('products/async/add_image', $params);
	}

	public function loadImages($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$images = $this->load()->model('Product_Images')->retrieve(['product_id' => $product_id]);

		$params = [
			'images' => $images
		];

		$this->load()->view('products/async/load_images', $params);
	}

	public function removeImage() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			$image = $this->load()->model('Product_images')->get_by_id($_POST['id']);

			if ($image->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);
	}

	public function loadWholesalePrices($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$prices = $this->load()->model('Product_Wholesale_Prices')->retrieve(['product_id' => $product_id]);

		$params = [
			'prices' => $prices
		];

		$this->load()->view('products/async/load_wholesale_prices', $params);
	}

	public function addWholesalePrice($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';//$this->index();
		}

		if ($_POST) {
			header("Content-type: application/json");

			$price_model = $this->load()->model('Product_Wholesale_Prices');
			$price_model->set_values($_POST);

			if ($id = $price_model->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$prices = $this->load()->model('Product_Wholesale_Prices')->retrieve(['product_id'=>$product->id]);

		$params = [
			'product' => $product,
			'prices' => $prices
		];

		$this->load()->view('products/async/add_wholesale_price', $params);
	}

	public function removeWholesalePrice() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			$price = $this->load()->model('Product_Wholesale_Prices')->get_by_id($_POST['id']);

			if ($price->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);
	}

	public function loadComponents($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("PC.*, P.name AS product_name");
		$db->from("Product_Components", "PC");
		$db->join("Products P", "P.id = PC.product_id1");
		$db->where("PC.product_id = '".$product->id."'");
		$components = $db->run();

		$params = [
			'components' => $components
		];

		$this->load()->view('products/async/load_components', $params);
	}

	public function addComponent($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';//$this->index();
		}

		if ($_POST) {
			header("Content-type: application/json");

			unset($_POST['product_name']);

			$component = $this->load()->model('Product_Components');
			$component->set_values($_POST);

			if ($id = $component->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$params = [
			'product' => $product
		];

		$this->load()->view('products/async/add_component', $params);
	}

	public function removeComponent() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			$component = $this->load()->model('Product_Components')->get_by_id($_POST['id']);

			if ($component->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);
	}

	public function componentProductSearch() {
		header("Content-type: application/json");

		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("*");
		$db->from("Products");
		$db->where("name LIKE '{$_GET['term']}%'");
		$db->limit(100);
		$return = [];
		if ($result = $db->run()) {
			foreach ($result as $key => $value) {
				$return[] = ['id'=>$value['id'], 'label'=>$value['name']];
			}
		}

		die(json_encode($return));
	}

	public function loadSizeColors($product_id) {
		auth('yes');

		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';
		}

		$tables_to_join = [
			'left' => [
				'colors' => 'C.id',
				'sizes' => 'S.id'
			]
		];
		$size_color_model = $this->load()->model('Size_Colors');
		$size_color_model->join()->prefix('SC');
		$size_color_model->join()->tables($tables_to_join);
		$size_color_model->join()->fields(['C.name'=>'color_name', 'S.name'=>'size_name']);
		$size_colors = $size_color_model->retrieve(['product_id' => $product_id]);

		$params = [
			'size_colors' => $size_colors
		];

		$this->load()->view('products/async/load_size_colors', $params);
	}

	public function addSizeColor($product_id) {
		auth('yes');
		
		if (!$product = $this->load()->model('Products')->get_by_id($product_id)) {
			return 'nah';//$this->index();
		}

		if ($_POST) {
			header("Content-type: application/json");

			$size_color_model = $this->load()->model('Size_Colors');
			$size_color_model->set_values($_POST);

			if ($id = $size_color_model->create()) {
				return $this->returnJson(200, $_POST, $id);
			}
			else {
				return $this->returnJson(500, $_POST);
			}
		}

		$sizes = $this->load()->model('Sizes')->retrieve('all', ['order'=>['name'=>'ASC']]);
		$colors = $this->load()->model('Colors')->retrieve('all', ['order'=>['name'=>'ASC']]);

		$params = [
			'product' => $product,
			'sizes' => $sizes,
			'colors' => $colors
		];

		$this->load()->view('products/async/add_size_color', $params);
	}

	public function removeSizeColor() {
		header("Content-type: application/json");
		auth('yes');

		if ($_POST) {
			$size_color = $this->load()->model('Size_Colors')->get_by_id($_POST['id']);

			if ($size_color->delete()) {
				return $this->returnJson(200);
			}
		}

		return $this->returnJson(500);
	}
}