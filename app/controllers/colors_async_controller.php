<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Colors_Async_Controller extends Core\App_Controller {
	public function add() {
		global $__post;
		if ($__post) {
			header("Content-type: application/json");

			$color_model = $this->load()->model('colors');
			$color_model->set_values($__post);

			if ($id = $color_model->create()) {
				return $this->returnJson(200, $__post, $id);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}
		$this->load()->view('colors/async/add');
	}

	public function update($id) {
		global $__post;

		$color = $this->load()->model('colors')->get_by_id($id);

		if ($__post) {
			header("Content-type: application/json");

			
			$color->set_values($__post);

			if ($id = $color->update()) {
				return $this->returnJson(200, $__post, $color);
			}
			else {
				return $this->returnJson(500, $__post);
			}
		}

		$params = [
			'color' => $color
		];

		$this->load()->view('colors/async/update', $params);
	}
}