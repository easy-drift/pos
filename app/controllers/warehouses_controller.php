<?php

namespace Application\Controllers;

use Core;
use Core\Helpers\Sql as Sql;

class Warehouses_Controller extends Core\App_Controller {
	public function index() {
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("W.*");
		$db->from("Warehouses", "W");
		$db->where("W.active IN (1,2)");
		$warehouses = $db->run();

		$params = [
			'warehouses' => $warehouses
		];

		$this->load()->view('warehouses/index',$params);
	}

	public function add() {
		global $__post;

		if ($__post) {
			$warehouse_model = $this->load()->model('Warehouses');

			$params = [
				'name' => $__post['name'],
				'active' => 1
			];

			$warehouse_model->set_values($params);

			if ($id = $warehouse_model->create()) {
				redirect_to('warehouses/index');
			}
			else {

			}
		}

		$this->load()->view('warehouses/add');
	}

	public function update() {
		global $__post;
		global $urlParams;

		$warehouse = $warehouse_model = $this->load()->model('Warehouses')->get_by_id($urlParams[0]);

		if ($__post) {
			$params = [
				'name' => $__post['name'],
			];

			$warehouse->set_values($params);

			if ($id = $warehouse->update()) {
				$CONFIG['msg']['success'][] = "Success";
			}
			else {
				$CONFIG['msg']['error'][] = "Error";
			}
		}

		$params = [
			'warehouse' => $warehouse,
		];

		$this->load()->view('warehouses/update',$params);
	}
}