<?php

namespace Application\Models;

use Core\App_Model as App_Model;
use Core\Helpers\Sql as Sql;

class Order_Model extends App_Model {
	var $id;
	var $person_id;
	var $user_id;
	var $pos_session_id;
	var $company_id;
	var $shipping_mtd_id;
	var $webshop_id;
	var $address_id;
	var $order_identify;
	var $pos_identify;
	var $date_shipped;
	var $tracking_code;
	var $date_cancelled;
	var $in_use;
	var $from_mobile;
	var $tip_value;
	var $value_shipping;
	var $discount;
	var $discount_reason;
	var $status;
	var $order_type;
	var $RefundNote;
	var $amount = 0.0;
	var $amount_paid = 0.0;
	var $amount_outstanding = 0.0;
	var $vat = 0.0;
	var $mobile;
	var $extra_info;
	var $card;
	var $viacliente;
	var $vialoja;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';

	function __construct() {
		global $CONFIG;
		parent::__construct();
		$this->order_identify = getOrderIdentify();
		$this->pos_identify = $CONFIG['device_id'];
	}

	public function getPayments() {
		$db = new Sql(get_pdo());
		$db->array_only = true;
		$db->select("OP.*, PM.name AS payment_method_name");
		$db->from("Order_Payments", "OP");
		$db->join("Payment_Methods PM", "PM.id = OP.payment_method_id");
		$db->where("OP.order_id = '".$this->id."'");
		$db->order("PM.id", "ASC");
		return $db->run();
	}
}