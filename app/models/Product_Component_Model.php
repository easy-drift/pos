<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Product_Component_Model extends App_Model {
	var $id;
	var $product_id;
	var $product_id1;
	var $name;
	var $qty;
	var $cost_price;
	var $retail_price;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}