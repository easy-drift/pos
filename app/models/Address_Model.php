<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Address_Model extends App_Model {
	var $id;
	var $zz_country_id;
	var $person_id;
	var $company_id;
	var $name;
	var $visiting_address;
	var $address_street;
	var $address_number;
	var $address_district;
	var $address_city;
	var $address_zipcode;
	var $address_complement;
	var $address_reference;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}