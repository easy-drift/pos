<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Product_Image_Model extends App_Model {
	var $id;
	var $product_id;
	var $path;
	var $order = '0';
	var $main = '0';
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}