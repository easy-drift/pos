<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Pos_Session_Closure_Model extends App_Model {
	var $id;
	var $user_id;
	var $pos_session_id;
	var $protocol;
	var $date;
	var $amount_counted;
	var $amount_received;
	var $amount_credit_card;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}