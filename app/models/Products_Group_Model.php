<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Products_Group_Model extends App_Model {
	var $id;
	var $products_group_id;
	var $name;
	var $printer;
	var $rules;
	var $department;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}