<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Ean_Code_Model extends App_Model {
	var $id;
	var $product_id;
	var $size_color_id;
	var $ean;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}