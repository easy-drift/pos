<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Products_Promotion_Model extends App_Model {
	var $id;
	var $product_id;
	var $gross_price;
	var $initial_date;
	var $price_without_tax;
	var $final_date;
	var $week = 0;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}