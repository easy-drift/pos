<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Favorite_Button_Model extends App_Model {
	var $id;
	var $favorite_button_tab_id;
	var $name;
	var $command;
	var $id_html;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}