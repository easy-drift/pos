<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Company_Model extends App_Model {
	var $id;
	var $pos_id;
	var $payment_mtd_id;
	var $name;
	var $fantasy_name;
	var $register_number;
	var $info;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = 1;
}