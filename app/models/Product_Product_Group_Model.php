<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Product_Product_Group_Model extends App_Model {
	var $id;
	var $product_id;
	var $product_group_id;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}