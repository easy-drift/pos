<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Meta_Model extends App_Model {
	var $id;
	var $data_type;
	var $data_value;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = 1;

	public function registerMeta($data_type, $data_value) {
		if ($this->get_by(['data_type'=>$data_type])) {
			$this->data_value = $data_value;
			$this->update();
		}
		else {
			$this->id = null;
			$this->data_type = $data_type;
			$this->data_value = $data_value;
			$this->create();
		}
	}
}