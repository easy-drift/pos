<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Payment_Method_Model extends App_Model {
	var $id;
	var $name;
	var $long_name;
	var $bbsid;
	var $account_number;
	var $path_platform;
	var $invoice = '1';
	var $deadlinemonth;
	var $deadlineyear;
	var $show_on_closure;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}