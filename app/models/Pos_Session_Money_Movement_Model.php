<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Pos_Session_Money_Movement_Model extends App_Model {
	var $id;
	var $pos_session_id;
	var $user_id;
	var $title;
	var $description;
	var $amount;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}