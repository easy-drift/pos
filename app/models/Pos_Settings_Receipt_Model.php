<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Pos_Settings_Receipt_Model extends App_Model {
	var $id;
	var $pos_id;
	var $data_type;
	var $data_value;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active;
}