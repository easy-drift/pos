<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Order_Payment_Model extends App_Model {
	var $id;
	var $payment_method_id;
	var $order_id;
	var $CreditNoteID;
	var $amount;
	var $change;
	var $amount_paid;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}