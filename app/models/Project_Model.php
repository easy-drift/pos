<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Project_Model extends App_Model {
	var $id;
	var $name;
	var $description;
	var $date_start;
	var $date_stop;
	var $date_end;
	var $price;
	var $priceAdditional;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = 1;
}