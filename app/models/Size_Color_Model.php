<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Size_Color_Model extends App_Model {
	var $id;
	var $product_id;
	var $color_id;
	var $size_id;
	var $codEan;
	var $codSupplier;
	var $total_amount;
	var $current_amount;
	var $price;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}