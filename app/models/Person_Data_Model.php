<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Person_Data_Model extends App_Model {
	var $id;
	var $person_id;
	var $data_type;
	var $data_value;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}