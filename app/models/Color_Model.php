<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Color_Model extends App_Model {
	var $id;
	var $name;
	var $code;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}