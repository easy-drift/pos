<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Person_Model extends App_Model {
	var $id;
	var $pos_id;
	var $customer_group_id;
	var $user_id;
	var $zz_country_id;
	var $person_setting_id;
	var $payment_method_id;
	var $department_id;
	var $name;
	var $document_type;
	var $document_number;
	var $birth_date;
	var $personal_number;
	var $contact_person;
	var $info;
	var $genre;
	var $electronic_identification;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}