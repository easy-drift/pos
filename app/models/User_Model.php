<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class User_Model extends App_Model {
	var $id;
	var $users_type_id;
	var $job_id;
	var $person_id;
	var $login;
	var $password;
	var $system_version;
	var $multiple_connections_limit;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}