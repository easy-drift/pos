<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Tax_Model extends App_Model {
	var $id;
	var $name;
	var $percent;
	var $mva_account;
	var $account_relapse;
	var $external_tax_code;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}