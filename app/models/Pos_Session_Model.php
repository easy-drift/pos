<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Pos_Session_Model extends App_Model {
	var $id;
	var $pos_id;
	var $user_id;
	var $date_start;
	var $end_date;
	var $cash_fund;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}