<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Person_Setting_Model extends App_Model {
	var $id;
	var $tax_free;
	var $inv_show_customer_name;
	var $inv_group_invoices;
	var $inv_tax_free;
	var $inv_invoice_print_exclusive;
	var $inv_hide_products;
	var $inv_electronic_invoice;
	var $inv_info;
	var $invoice_tmpt_id;
	var $inv_email;
	var $inv_track_email;
	var $currency_id;
	var $person_id;
	var $ord_show_info;
	var $ord_hide_from_statistics;
	var $ord_use_com_address;
	var $ticket_tmpt_id;
	var $project_id;
	var $ord_credit_limit;
	var $ord_payment_terms;
	var $charge_times;
	var $language_id;
	var $auth_receive_sms;
	var $auth_receive_email;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}




    				 	   		