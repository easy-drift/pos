<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Pos_Model extends App_Model {
	var $id;
	var $user_id;
	var $nickname;
	var $macAddress;
	var $about;
	var $idlicense;
	var $ticket_tmpt_idticket_tmpt;
	var $temp_ticket_tmpt_idtemp_ticket_tmpt;
	var $pos_number;
	var $valid_until;
	var $qty_users;
	var $qty_waiters;
	var $last_online;
	var $pos_company_idpos_company;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}