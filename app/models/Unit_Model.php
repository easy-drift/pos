<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Unit_Model extends App_Model {
	var $id;
	var $unit_id;
	var $name;
	var $divisible;
	var $ratio;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}