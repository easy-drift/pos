<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Order_Line_Model extends App_Model {
	var $id;
	var $order_id;
	var $product_id;
	var $size_color_id;
	var $user_id;
	var $warranty_id;
	var $value_warranty;
	var $date_warranty;
	var $extra_info;
	var $value;
	var $actual_value;
	var $tax;
	var $qty;
	var $discount;
	var $discount_reason;
	var $returned;
	var $returned_reason;
	var $date_return;
	var $is_gift = '0';
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}