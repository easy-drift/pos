<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Product_Group_Model extends App_Model {
	var $id;
	var $product_group_id;
	var $department_id;
	var $name;
	var $printer;
	var $rules;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}