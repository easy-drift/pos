<?php

namespace Application\Models;

use Core\App_Model as App_Model;

class Zz_Country_Model extends App_Model {
	var $id;
	var $code;
	var $name;
	var $date_create;
	var $date_update;
	var $date_delete;
	var $active = '1';
}